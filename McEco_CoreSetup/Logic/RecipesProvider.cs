﻿using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using McEco.CoreSetup.Configs;
using McEco.Lib.Helper;
using System;

namespace McEco.CoreSetup.Logic
{
    internal sealed class RecipesProvider
    {
        public static void Update(StaticConfig config)
        {
            foreach (RecipeFamily recipe in RecipeFamily.AllRecipes)
            {
                if (config.Recipe.CraftMinutesMultiplier != 1f)
                {
                    float newValue = Math.Max(0f, recipe.CraftMinutes.GetBaseValue * config.Recipe.CraftMinutesMultiplier);
                    Tools.GetSetterForProperty<RecipeFamily, IDynamicValue>(x => x.CraftMinutes)?.Invoke(recipe, new ConstantValue(newValue));
                }
                if (config.Recipe.LaborInCaloriesMultiplier != 1f)
                {
                    float newValue = Math.Max(0f, recipe.LaborInCalories.GetBaseValue * config.Recipe.LaborInCaloriesMultiplier);
                    Tools.GetSetterForProperty<RecipeFamily, IDynamicValue>(x => x.LaborInCalories)?.Invoke(recipe, new ConstantValue(newValue));
                }
                if (config.Recipe.ExperienceOnCraftMultiplier != 1f)
                {
                    recipe.ExperienceOnCraft = Math.Max(0f, recipe.ExperienceOnCraft * config.Recipe.ExperienceOnCraftMultiplier);
                }
                if (config.Recipe.RequiresSkillLevelForce != null)
                {
                    RequiresSkillAttribute[] attrs = RequiresSkillAttribute.Cache.Get(recipe.GetType());
                    if (attrs?.Length > 0)
                    {
                        RequiresSkillAttribute attr = attrs[0];
                        int level = config.Recipe.RequiresSkillLevelForce ?? int.MaxValue;
                        if (level != int.MaxValue)
                        {
                            int newLevel = Math.Max(0, Math.Min(level, 7));
                            Tools.GetSetterForProperty<RequiresSkillAttribute, int>(x => x.Level)?.Invoke(attr, newLevel);
                        }
                    }
                }
                if (config.Recipe.IngredientsQuantityMultiplier != 1f)
                {
                    foreach (IngredientElement ingredient in recipe.Ingredients)
                    {
                        float newValue = Math.Max(0f, ingredient.Quantity.GetBaseValue * config.Recipe.IngredientsQuantityMultiplier);
                        Tools.GetSetterForProperty<IngredientElement, IDynamicValue>(x => x.Quantity)?.Invoke(ingredient, new ConstantValue(newValue));
                    }
                }
                if (config.Recipe.ProductsQuantityMultiplier != 1f)
                {
                    foreach (CraftingElement craft in recipe.Product)
                    {
                        float newValue = Math.Max(0f, craft.Quantity.GetBaseValue * config.Recipe.ProductsQuantityMultiplier);
                        Tools.GetSetterForProperty<CraftingElement, IDynamicValue>(x => x.Quantity)?.Invoke(craft, new ConstantValue(newValue));
                    }
                }
            }
        }
    }
}
