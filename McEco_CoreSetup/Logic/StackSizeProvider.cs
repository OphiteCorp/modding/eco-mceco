﻿using Eco.Gameplay.Items;
using Eco.Shared.Utils;
using McEco.CoreSetup.Configs;
using System.Reflection;

namespace McEco.CoreSetup.Logic
{
    internal sealed class StackSizeProvider
    {
        public static void StaticUpdate(StaticConfig config)
        {
            MaxStackSizeAttribute.Default = config.StackSize.DefaultStackSize;
        }

        public static void Update(StaticConfig config)
        {
            if (config.StackSize.IsOverrideOnItems)
            {
                Item.AllItems.ForEach(item =>
                {
                    var property = typeof(MaxStackSizeAttribute)?.GetProperty("MaxStackSize", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    if (property != null && ItemAttribute.TryGet<MaxStackSizeAttribute>(item.Type, out MaxStackSizeAttribute itemAttribute))
                    {
                        property?.SetValue(itemAttribute, config.StackSize.DefaultStackSize);
                    }
                });
            }
        }
    }
}
