﻿using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Mods.TechTree;
using Eco.Shared.Localization;
using McEco.CoreSetup.Configs;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace McEco.CoreSetup.Logic
{
    internal sealed class PlayerDefaultsProvider
    {
        public static void StaticUpdate(StaticConfig config)
        {
            Dictionary<UserStatType, IDynamicValue> dictionary = PlayerDefaults.GetDefaultDynamicValues();

            if (config.Stomach.BaseCapacityMultiplier != 1f || config.Stomach.Multiplier != 1f)
            {
                dictionary[UserStatType.MaxCalories] = CreateMaxCalories(config.Stomach.BaseCapacityMultiplier, config.Stomach.Multiplier);
            }
            if (config.MaxCarryWeightMultiplier != 1f)
            {
                dictionary[UserStatType.MaxCarryWeight] = CreateMaxCarryWeight(config.MaxCarryWeightMultiplier);
            }
            if (config.MaxAnimalsApproach != 0f)
            {
                dictionary[UserStatType.DetectionRange] = CreateDetectionRange(config.MaxAnimalsApproach);
            }
            dictionary[UserStatType.MovementSpeed] = CreateMovementSpeed(config.IsMovementSpeedMaxed);

            var field = typeof(PlayerDefaults)?.GetField("dynamicValuesDictionary", BindingFlags.Static | BindingFlags.NonPublic);
            field?.SetValue(null, dictionary);
        }

        private static MultiDynamicValue CreateMaxCalories(float baseMultiplier = 1.4f, float multiplier = 12.7f)
        {
            return new MultiDynamicValue(MultiDynamicOps.Sum,
                    new MultiDynamicValue(MultiDynamicOps.Multiply, CreateSmv(0f, new BonusUnitsDecoratorStrategy(SelfImprovementSkill.AdditiveStrategy, "cal", (float val) => val / 2f), typeof(SelfImprovementSkill), Localizer.DoStr("stomach capacity"), DynamicValueType.Misc),
                    new ConstantValue(0.5f * multiplier)), // pokud bude 6.35, tak bude mit zaludek ve vysledku zhruba 20k kalorii
                    new TalentModifiedValue(typeof(UserStatType), typeof(SelfImprovementGluttonTalent), 0f),
                    new ConstantValue(3000f * baseMultiplier)); // pokud bude 4200, tak bude mit zaludek ve vysledku zhruba 20k kalorii
        }

        private static MultiDynamicValue CreateMovementSpeed(bool maximum = true)
        {
            return maximum ?
                new MultiDynamicValue(MultiDynamicOps.Sum, new ConstantValue(1000f)) :
                new MultiDynamicValue(MultiDynamicOps.Sum, new TalentModifiedValue(typeof(UserStatType), typeof(SelfImprovementNatureAdventurerSpeedTalent), 0f), new TalentModifiedValue(typeof(UserStatType), typeof(SelfImprovementUrbanTravellerSpeedTalent), 0f));
        }

        private static MultiDynamicValue CreateMaxCarryWeight(float multiplier = 10f)
        {
            return new MultiDynamicValue(MultiDynamicOps.Sum,
                CreateSmv(0f, new BonusUnitsDecoratorStrategy(SelfImprovementSkill.AdditiveStrategy, "kg", (float val) => val / 1000f), typeof(SelfImprovementSkill), Localizer.DoStr("carry weight"), DynamicValueType.Misc),
                new TalentModifiedValue(typeof(UserStatType), typeof(SelfImprovementDeeperPocketsTalent), 0),
                new ConstantValue(ToolbarBackpackInventory.DefaultWeightLimit * multiplier));
        }

        private static MultiDynamicValue CreateDetectionRange(float startValue)
        {
            return new MultiDynamicValue(MultiDynamicOps.Sum,
                CreateSmv(startValue, HuntingSkill.AdditiveStrategy, typeof(HuntingSkill), Localizer.DoStr("how close you can approach animals"), DynamicValueType.Misc),
                new ConstantValue(startValue));
        }

        private static SkillModifiedValue CreateSmv(float startValue, ModificationStrategy strategy, Type skillType, LocString benefitsDescription, DynamicValueType valueType)
        {
            var smv = new SkillModifiedValue(startValue, strategy, skillType, benefitsDescription, valueType);
            SkillModifiedValueManager.AddSkillBenefit(skillType, smv, typeof(Player));
            return smv;
        }
    }
}
