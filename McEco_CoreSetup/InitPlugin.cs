﻿using Eco.Core;
using Eco.Core.Plugins;
using Eco.Core.Utils;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Skills;
using Eco.Gameplay.Systems.Messaging.Notifications;
using Eco.Gameplay.Utils;
using Eco.Mods.TechTree;
using Eco.Plugins.Networking;
using Eco.Server;
using Eco.Shared.Localization;
using Eco.Shared.Math;
using Eco.Shared.Networking;
using Eco.Shared.Utils;
using Eco.Simulation.WorldLayers;
using Eco.World;
using Eco.World.Blocks;
using McEco.CoreSetup.Configs;
using McEco.CoreSetup.Logic;
using McEco.Lib;
using McEco.Lib.Ext;
using McEco.Lib.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;

namespace McEco.CoreSetup
{
    [LocDisplayName(DefaultModuleName)]
    [Priority(PriorityAttribute.VeryLow)]
    public class InitPlugin : AbstractInitConfigurablePlugin
    {
        public const string DefaultModuleName = "CoreSetup";

        private readonly PluginConfig<Config> config = new(DefaultCategory + DefaultModuleName);

        public static InitPlugin Obj => Eco.Core.PluginManager.GetPlugin<InitPlugin>();

        public Config Config => config.Config;

        public static StaticConfig StaticConfig { get; set; }

        public override IPluginConfig PluginConfig => config;

        public InitPlugin()
        {
            ModuleName = DefaultModuleName;
        }

        public override object GetEditObject()
        {
            return config.Config;
        }

        public override void OnEditObjectChanged(object o, string param)
        {
            config.SaveAsync().Wait();
        }

        static InitPlugin() {
            StaticConfig = StaticConfig.Load($"Configs/{DefaultCategory}{DefaultModuleName}.json");

            PlayerDefaultsProvider.StaticUpdate(StaticConfig);
            StackSizeProvider.StaticUpdate(StaticConfig);
        }

        protected override void StartUp(TimedTask timed)
        {
            StackSizeProvider.Update(StaticConfig);
            RecipesProvider.Update(StaticConfig);
        }

        public override string GetDisplayText()
        {
            StringBuilder sb = new();
            sb.AppendLine($"Stomach - BaseCapacityMultiplier: {StaticConfig.Stomach.BaseCapacityMultiplier}x");
            sb.AppendLine($"Stomach - Multiplier: {StaticConfig.Stomach.Multiplier}x");
            sb.AppendLine("------------------------------------------------");
            sb.AppendLine($"StackSize - DefaultStackSize: {StaticConfig.StackSize.DefaultStackSize}");
            sb.AppendLine($"StackSize - IsOverrideOnItems: {StaticConfig.StackSize.IsOverrideOnItems}");
            sb.AppendLine("------------------------------------------------");
            sb.AppendLine($"Recipe - IngredientsQuantityMultiplier: {StaticConfig.Recipe.IngredientsQuantityMultiplier}x");
            sb.AppendLine($"Recipe - ProductsQuantityMultiplier: {StaticConfig.Recipe.ProductsQuantityMultiplier}x");
            sb.AppendLine($"Recipe - CraftMinutesMultiplier: {StaticConfig.Recipe.CraftMinutesMultiplier}x");
            sb.AppendLine($"Recipe - LaborInCaloriesMultiplier: {StaticConfig.Recipe.LaborInCaloriesMultiplier}x");
            sb.AppendLine($"Recipe - ExperienceOnCraftMultiplier: {StaticConfig.Recipe.ExperienceOnCraftMultiplier}x");
            sb.AppendLine($"Recipe - RequiresSkillLevelForce: {StaticConfig.Recipe.RequiresSkillLevelForce}");
            sb.AppendLine("------------------------------------------------");
            sb.AppendLine($"MaxCarryWeightMultiplier: {StaticConfig.MaxCarryWeightMultiplier}x");
            sb.AppendLine($"IsMovementSpeedMaxed: {StaticConfig.IsMovementSpeedMaxed}");
            sb.AppendLine($"MaxAnimalsApproach: {StaticConfig.MaxAnimalsApproach}");
            sb.AppendLine("------------------------------------------------");
            sb.AppendLine("by mimicz");
            return sb.ToString();
        }
    }
}