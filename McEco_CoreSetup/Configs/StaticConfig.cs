﻿using Eco.Gameplay.Items;
using System.Text.Json;

namespace McEco.CoreSetup.Configs
{
    public sealed class StaticConfig
    {
        public StomachConf Stomach { get; set; } = new();
        public StackSizeConf StackSize { get; set; } = new();
        public RecipeConf Recipe { get; set; } = new();
        public float MaxCarryWeightMultiplier { get; set; } = 1f; // hmotnost batohu
        public bool IsMovementSpeedMaxed { get; set; } = false; // pokud bude True, hrac bude vzdy bezet maximalni rychlosti i bez shift
        public float MaxAnimalsApproach { get; set; } = 0f; // 0-10 (cim vetsi cislo, tim mene se zvirata nebudou bat)

        public sealed class StomachConf
        {
            public float BaseCapacityMultiplier { get; set; } = 1f; // zakladni kapacita.. idealni: 1.4f
            public float Multiplier { get; set; } = 1f; // nasobic za level.. idealni: 12.7f
        }

        public sealed class StackSizeConf
        {
            public int DefaultStackSize { get; set; } = MaxStackSizeAttribute.Default;
            public bool IsOverrideOnItems { get; set; } = false;
        }

        public sealed class RecipeConf
        {
            public float CraftMinutesMultiplier { get; set; } = 1f;
            public float LaborInCaloriesMultiplier { get; set; } = 1f;
            public float IngredientsQuantityMultiplier { get; set; } = 1f;
            public float ProductsQuantityMultiplier { get; set; } = 1f;
            public float ExperienceOnCraftMultiplier { get; set; } = 1f;
            public int? RequiresSkillLevelForce { get; set; } = null;
        }

        public void Save(string file)
        {
            var json = JsonSerializer.Serialize(this, new JsonSerializerOptions
            {
                WriteIndented = true
            });
            File.WriteAllText(file, json);
        }

        public static StaticConfig Load(string file)
        {
            if (File.Exists(file))
            {
                var json = File.ReadAllText(file);
                return JsonSerializer.Deserialize<StaticConfig>(json) ?? new StaticConfig();
            }
            else
            {
                StaticConfig defaultConfig = new();
                var json = JsonSerializer.Serialize(defaultConfig, new JsonSerializerOptions
                {
                    WriteIndented = true
                });
                File.WriteAllText(file, json);
                return defaultConfig;
            }
        }
    }
}
