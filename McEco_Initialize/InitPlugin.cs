﻿using Eco.Core;
using Eco.Core.Controller;
using Eco.Core.Plugins;
using Eco.Core.Utils;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Skills;
using Eco.Gameplay.Systems.Messaging.Notifications;
using Eco.Mods.TechTree;
using Eco.Shared.IoC;
using Eco.Shared.Localization;
using Eco.Shared.Math;
using Eco.Shared.Networking;
using Eco.Shared.Utils;
using Eco.Simulation.WorldLayers;
using Eco.World;
using Eco.World.Blocks;
using McEco.Initialize.Configs;
using McEco.Lib;
using McEco.Lib.Ext;
using McEco.Lib.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;

namespace McEco.Initialize
{
    [Priority(PriorityAttribute.VeryHigh)]
    public class InitPlugin : AbstractInitConfigurablePlugin
    {
        public const string DefaultModuleName = "Initialize";

        private readonly PluginConfig<Config> config = new(DefaultCategory + DefaultModuleName);

        public static InitPlugin Obj => PluginManager.GetPlugin<InitPlugin>();

        public Config Config => config.Config;

        public override IPluginConfig PluginConfig => config;

        public InitPlugin()
        {
            ModuleName = DefaultModuleName;
        }

        public override object GetEditObject()
        {
            return config.Config;
        }

        public override void OnEditObjectChanged(object o, string param)
        {
            config.SaveAsync().Wait();
        }

        protected override void StartUp(TimedTask timed)
        {
            typeof(TailingsItem).SetCustomStackSize(50);
            typeof(WetTailingsItem).SetCustomStackSize(50);
            typeof(WoodPulpItem).SetCustomStackSize(250);


            UserManager.NewUserJoinedEvent.Add(u =>
            {
                if (u.SlgId == "DiscordLinkSlg")
                {
                    return;
                }
                if (Obj.Config.AutoLearnInitSkills)
                {
                    LearnFirstSkills(u);
                    RemoveAllAvailableStars(u);
                }
                if (Obj.Config.BonusStars > 0)
                {
                    AddBonusStars(u, Obj.Config.BonusStars);
                }
                //AddStartItems(u, "WoodenBigShovelItem");
            });
            UserManager.OnUserLoggedIn.Add(u =>
            {
                //LearnAllSkillsToLevel(u, 1);
                //AddListenerForRemoveStar(u);
                //UpdateStartStars(u);
            });
        }

        public override string GetDisplayText()
        {
            float totalCo2 = (float)Math.Round(WorldLayerManager.Obj.Climate.TotalCO2, 2);
            float seaLevel = (float)Math.Round(Singleton<WorldLayerManager>.Obj.Climate.SeaLevel, 2);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Current Status: " + GetStatus());
            sb.AppendLine();
            sb.AppendLine($"Total CO²: {totalCo2} ppm");
            sb.AppendLine($"Sea Level: {seaLevel} meters");

            return sb.ToString();
        }

        private static void LearnAllSkillsToLevel(User user, int skillLevel)
        {
            Item.AllItems.OfType<SkillScroll>().ForEach(scroll =>
            {
                if (!user.Skillset.HasSkill(scroll.Skill.Type))
                {
                    user.Skillset.LearnSkill(scroll.Skill.Type);
                    //SkillUtils.LearnSkillAndGiveClaims(user, scroll.Skill);
                }
            });
            foreach (Skill skill in user.Skillset.Skills)
            {
                if (skill.Level == 0)
                {
                    skill.ForceSetLevel(user, skillLevel);
                }
            }
            user.UserXP.StarsAvailable = 0;
        }

        private static void UpdateStartStars(User user)
        {
            user.UserXP.StarsAvailable = 1;

            user.UserXP.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "StarsAvailable" || args.PropertyName == "TotalStarsEarned")
                {
                    if (sender is UserXP xp)
                    {
                        if (xp.StarsAvailable < 1)
                        {
                            xp.StarsAvailable = 1;
                        }
                    }
                }
            };
        }

        private static void LearnFirstSkills(User user)
        {
            user.Skillset.Skills.ForEach(s =>
            {
                s.Learn(user, true, true, false);
            });
            user.Skillset.Skills.ForEach(s =>
            {
                s.ForceSetLevel(user, 1);
            });
        }

        private static void RemoveAllAvailableStars(User user)
        {
            user.UserXP.StarsAvailable = 0;
        }

        private static void AddStartItems(User user, params string[] itemCodes)
        {
            foreach (string code in itemCodes)
            {
                Item item = Item.Get(code);
                if (item != null)
                {
                    user.Inventory.AddItem(item.Clone);
                }
            }
        }

        private static void AddBonusStars(User user, int stars)
        {
            user.UserXP.AddStars(stars);
        }

        private static void AddListenerForRemoveStar(User user)
        {
            user.UserXP.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "StarsAvailable")
                {
                    if (sender is UserXP xp)
                    {
                        if (xp.StarsAvailable > 0)
                        {
                            xp.StarsAvailable = 0;
                        }
                    }
                }
            };
        }
    }
}