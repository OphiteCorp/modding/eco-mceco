﻿using System.ComponentModel;

namespace McEco.Initialize.Configs
{
    [DisplayName("Configuration")]
    public class Config
    {
        [DisplayName("Automatically learns initial skills")]
        [Description("Valid for new players only. It also removes all initial stars. For new players only!")]
        public bool AutoLearnInitSkills { get; set; } = false;

        [DisplayName("Bonus stars")]
        [Description("Valid for new players only. Adds extra stars to the start. For new players only!")]
        public int BonusStars { get; set; } = 0;
    }
}
