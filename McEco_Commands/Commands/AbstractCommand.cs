﻿using Eco.Gameplay.Items;

namespace McEco.Commands.Commands
{
    internal abstract class AbstractCommand
    {
        protected const string VALUE_COLOR = "#DAB287";

        protected static string CreateTitleLine(string name)
        {
            //char ch = '=';
            //string startLine = new(ch, 5);
            return $"[ <color=#FB5686>{name}</color> ]\n";
        }

        protected static string CreateSubTitleLine(string name)
        {
            //char ch = '-';
            //string startLine = new(ch, 5);
            return $"[ <color=#F8AC6A>{name}</color> ]\n";
        }

        protected static int SumItems(ItemStack[] stacks)
        {
            int sum = 0;
            foreach (ItemStack stack in stacks)
            {
                sum += stack.Quantity;
            }
            return sum;
        }
    }
}
