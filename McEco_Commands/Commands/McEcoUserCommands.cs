﻿using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Messaging.Chat.Commands;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Mods.TechTree;
using Eco.Plugins.Networking;
using Eco.Shared.Math;
using Eco.Shared.Utils;
using Eco.Simulation.WorldLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McEco.Lib.Ext;

namespace McEco.Commands.Commands
{
    [ChatCommandHandler]
    internal sealed class McEcoUserCommands : AbstractCommand
    {
        private const string DefaultCommandName = "McEcoUser";

        private static readonly Dictionary<string, long> users = new();

        [ChatCommand(DefaultCommandName + " Commands")]
        public static void McEcoUser()
        {
        }

        [ChatSubCommand(DefaultCommandName, "Better unstuck.", "unstuck2", ChatAuthorizationLevel.User)]
        public static void BetterUnstuck(User user)
        {
            if (users.ContainsKey(user.Name))
            {
                long diff = DateTime.Now.Ticks - users[user.Name];
                TimeSpan elapsed = new(diff);
                int delay = InitPlugin.Obj.Config.BetterUnstuckDelay;

                if ((int)elapsed.TotalSeconds < delay)
                {
                    user.MsgLocStr($"The next use of this command will be possible in {(int)(delay - elapsed.TotalSeconds)}s.");
                    return;
                }
            }
            if (user.Player.Mount.IsMounted)
            {
                user.Player.Mount.Mount.Dismount(user.Player);
            }
            var pos = user.Position.XYZi();
            AdminCommands.ToWorldPosition(user, pos.X, 300, pos.Z);
            user.MsgLocStr("You are free now!");

            users[user.Name] = DateTime.Now.Ticks;
        }

        [ChatSubCommand(DefaultCommandName, "Show world status.", "status", ChatAuthorizationLevel.User)]
        public static void WorldStatus(User user)
        {
            var climate = WorldLayerManager.Obj.Climate;
            var colorCo2 = (climate.TotalCO2 >= 420) ? "red" : "green";

            string msg = "";
            var tailingsCount = McEcoAdminCommands.CountOfItemInternal(user, "Tailings", ref msg);
            var wetTailingsCount = McEcoAdminCommands.CountOfItemInternal(user, "Wet Tailings", ref msg);
            var ironBarCount = McEcoAdminCommands.CountOfItemInternal(user, "Iron Bar", ref msg);
            var copperBarCount = McEcoAdminCommands.CountOfItemInternal(user, "Copper Bar", ref msg);
            var goldBarCount = McEcoAdminCommands.CountOfItemInternal(user, "Gold Bar", ref msg);
            var steelBarCount = McEcoAdminCommands.CountOfItemInternal(user, "Steel Bar", ref msg);

            var info = NetworkManager.GetServerInfo();

            StringBuilder sb = new();
            sb.Append($"<color={VALUE_COLOR}>Temperature:</color> {Math.Round(climate.AverageGlobalTemperature, 2)}\n");
            sb.Append($"<color={VALUE_COLOR}>Sea Level:</color> {Math.Round(climate.SeaLevel, 2)}\n");
            sb.Append($"<color={VALUE_COLOR}>Ground Pollution:</color> {Math.Round(climate.TotalGroundPollution, 2)}\n");
            sb.Append("-------------------------------------------\n");
            sb.Append($"<color={VALUE_COLOR}>CO² Citizen Activity:</color> {Math.Round(climate.CO2FromCitizenActivityPpm, 2)} ppm\n");
            sb.Append($"<color={VALUE_COLOR}>CO² Animals:</color> {Math.Round(climate.CO2FromAnimalsPpm, 2)} ppm\n");
            sb.Append($"<color={VALUE_COLOR}>CO² Plants:</color> {Math.Round(climate.CO2FromPlantsPpm, 2)} ppm\n");
            sb.Append($"<color={VALUE_COLOR}>CO² Total:</color> <color={colorCo2}>{Math.Round(climate.TotalCO2, 2)}</color> ppm\n");
            sb.Append("-------------------------------------------\n");
            sb.Append($"<color={VALUE_COLOR}>Animals:</color> {info.Animals}\n");
            sb.Append($"<color={VALUE_COLOR}>Plants:</color> {info.Plants}\n");
            sb.Append($"<color={VALUE_COLOR}>Laws:</color> {info.Laws}\n");
            sb.Append("-------------------------------------------\n");
            sb.Append($"<color={VALUE_COLOR}>Total Tailings:</color> {tailingsCount + wetTailingsCount} (<color={VALUE_COLOR}>Dry:</color> {tailingsCount}, <color={VALUE_COLOR}>Wet:</color> {wetTailingsCount})\n");
            sb.Append($"<color={VALUE_COLOR}>Total Iron Bars:</color> {ironBarCount}\n");
            sb.Append($"<color={VALUE_COLOR}>Total Copper Bars:</color> {copperBarCount}\n");
            sb.Append($"<color={VALUE_COLOR}>Total Gold Bars:</color> {goldBarCount}\n");
            sb.Append($"<color={VALUE_COLOR}>Total Steel Bars:</color> {steelBarCount}\n");

            user.SendInfoBox("World Status", sb.ToString());
        }

        [ChatSubCommand(DefaultCommandName, "Show server Discord link.", "discord", ChatAuthorizationLevel.User)]
        public static void DiscordLink(User user)
        {
            string link = NetworkManager.GetServerInfo().DiscordAddress;
            user.SendInfoBox("Discord", $"<size=30>{link}</size>");
        }

        [ChatSubCommand(DefaultCommandName, "Searches for all saved tailings that are above a certain height on the map.", "tailings", ChatAuthorizationLevel.User)]
        public static void FindTailings(User user, int aboveY = 0)
        {
            StringBuilder sb = new();
            var tailingsItem = Item.Get(typeof(TailingsItem));
            var wetTailingsItem = Item.Get(typeof(WetTailingsItem));
            int total = 0;

            IEnumerable<StorageComponent> storages = WorldObjectUtil.AllObjsWithComponent<StorageComponent>();
            storages.OrderBy(p => p.Parent.Position.XYZi().Y).ThenBy(p => p.Parent.Name).ForEach(storage =>
            {
                bool hasOwner = storage.Owners?.UserSet?.Any() ?? false;
                var owner = hasOwner ? storage.Parent.Owners.UILinkGeneric() : new Eco.Shared.Localization.LocString("<color=red>[public]</color>");

                var position = storage.Parent.Position.XYZi();
                int loopSum = 0;

                if (position.Y > aboveY)
                {
                    ItemStack[] dryStacks = storage.Inventory.Stacks.Where(slot => slot.Item != null && slot.Item.GetType().IsEquivalentTo(tailingsItem.GetType())).ToArray();
                    if (dryStacks.Length > 0)
                    {
                        int sum = SumItems(dryStacks);
                        sb.Append($"{owner} have <color={VALUE_COLOR}>{dryStacks.Length}</color> stacks (Total: <color={VALUE_COLOR}>{sum}</color>) in {storage.Parent.UILink()} (<color={VALUE_COLOR}>Deep:</color> {position.Y} m)\n");
                        loopSum += sum;
                    }
                    ItemStack[] wetStacks = storage.Inventory.Stacks.Where(slot => slot.Item != null && slot.Item.GetType().IsEquivalentTo(wetTailingsItem.GetType())).ToArray();
                    if (wetStacks.Length > 0)
                    {
                        int sum = SumItems(wetStacks);
                        sb.Append($"{owner} have <color={VALUE_COLOR}>{wetStacks.Length}</color> stacks (Total: <color={VALUE_COLOR}>{sum}</color>) in {storage.Parent.UILink()} (<color={VALUE_COLOR}>Deep:</color> {position.Y} m)\n");
                        loopSum += sum;
                    }
                }
                total += loopSum;
            });

            if (total == 0)
            {
                sb.Append($"<color=green>Everything is fine</color> and all the tailings are stored deep underground.\n");
            }
            user.SendInfoBox("World Tailings", sb.ToString());
        }
    }
}
