﻿using Eco.Gameplay.Aliases;
using Eco.Gameplay.Civics.Demographics;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Property;
using Eco.Gameplay.Systems.Messaging.Chat.Commands;
using Eco.Gameplay.Systems.Messaging.Notifications;
using Eco.Gameplay.Systems.NewTooltip.TooltipLibraryFiles;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Gameplay.Utils;
using Eco.Mods.TechTree;
using Eco.Plugins.Networking;
using Eco.Shared.IoC;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using Eco.Shared.Voxel;
using Eco.Simulation.Time;
using McEco.Lib.Ext;
using McEco.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Reflection.Metadata;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace McEco.Commands.Commands
{
    [ChatCommandHandler]
    internal sealed class McEcoAdminCommands : AbstractCommand
    {
        private const string DefaultCommandName = "McEcoAdmin";

        [ChatCommand(DefaultCommandName + " Commands")]
        public static void McEcoAdmin()
        {
        }

        [ChatSubCommand(DefaultCommandName, "Resets world time. (Carefully!)", "resetworldtime", ChatAuthorizationLevel.Admin)]
        public static void ResetWTime(User user)
        {
            WorldTime.ResetOffset();
            WorldTime.Reset();

            user.MsgLocStr("World time has been reset to initial values.");
        }

        [ChatSubCommand(DefaultCommandName, "Adds or subtracts world time. (Carefully!)", "updateworldtime", ChatAuthorizationLevel.Admin)]
        public static void AddWTime(User user, int seconds)
        {
            WorldTime.ForceAdvanceTime(seconds);

            var span = TimeSpan.FromSeconds(seconds);
            user.MsgLocStr($"Server time changed by: {span.ToFormatted()}");
        }

        [ChatCommand("Sets The Worlds Date To Selected Day, You can also specify true or false to grant accumulated xp if you choose to advance the time", ChatAuthorizationLevel.Admin)]
        public static void SetDayTime(User user, int day = 0, bool grantXp = false)
        {
            WorldTime.Reset();
            Log.WriteWarningLineLocStr("World Time Reset");
            double newtime = TimeUtil.DaysToSeconds(day);
            if (grantXp)
                WorldTime.ForceAdvanceTime(newtime);
            else
            {
                double fakeStartTime = TimeUtil.Seconds - newtime;
                WorldTime.Obj.GetType().GetField("realTimeAtLoad", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).SetValue(WorldTime.Obj, fakeStartTime);
            }
            WorldTime.TimeChanged();
            Log.WriteWarningLineLocStr($"World Time Set To Day: {day}");
            user.Player.OkBoxLoc($"World Time was Reset: It is now Day {day}");
        }

        #region Get/Send/Repair Item

        [ChatSubCommand(DefaultCommandName, "Add item to inventory.", "get", ChatAuthorizationLevel.Admin)]
        public static void GetItem(User user, string itemName, int quantity = 1)
        {
            Item item = itemName.ClosestMatchingEntity(user);
            if (item == null)
            {
                user.MsgLocStr($"The item name <color={VALUE_COLOR}>{itemName}</color> does not exist in the game.");
                return;
            }
            var result = user.Inventory.TryAddItems(item.Type, quantity);
            if (result.Failed)
            {
                user.MsgLocStr($"<color={VALUE_COLOR}>{quantity}x {itemName}</color> cannot be placed in full inventory.");
            }
            else
            {
                user.MsgLocStr($"<color={VALUE_COLOR}>{quantity}x {itemName}</color> have been added.");
            }
        }

        [ChatSubCommand(DefaultCommandName, "Add item to inventory with no quantity limit.", "fget", ChatAuthorizationLevel.Admin)]
        public static void ForceGetItem(User user, string itemName, int quantity = 1)
        {
            Item item = itemName.ClosestMatchingEntity(user);
            if (item == null)
            {
                user.MsgLocStr($"The item name <color={VALUE_COLOR}>{itemName}</color> does not exist in the game.");
                return;
            }
            AdminCommands.ForceGive(user, item, quantity);
        }

        [ChatSubCommand(DefaultCommandName, "Add an item to another player's inventory.", "getto", ChatAuthorizationLevel.Admin)]
        public static void GetItemTo(User user, User targetUser, string itemName, int quantity = 1)
        {
            Item item = itemName.ClosestMatchingEntity(targetUser);
            if (item == null)
            {
                user.MsgLocStr($"The item name <color={VALUE_COLOR}>{itemName}</color> does not exist in the game.");
                return;
            }
            var result = targetUser.Inventory.TryAddItems(item.Type, quantity);
            if (result.Failed)
            {
                user.MsgLocStr($"<color={VALUE_COLOR}>{quantity}x {itemName}</color> cannot be placed in full inventory.");
            }
            else
            {
                user.MsgLocStr($"<color={VALUE_COLOR}>{quantity}x {itemName}</color> have been added to the <color=green>{targetUser.Name} inventory</color>.");
                targetUser.MsgLocStr($"User <color=green>{user.Name}</color> sent <color={VALUE_COLOR}>{quantity}x {itemName}</color> to your inventory.");
            }
        }

        [ChatSubCommand(DefaultCommandName, "Repairs all tools in your toolbar.", "repair", ChatAuthorizationLevel.Admin)]
        public static void RepairTools(User user)
        {
            var stacks = user.Inventory?.Toolbar?.NonEmptyStacks ?? null;
            if (stacks != null)
            {
                stacks.ForEach(s =>
                {
                    if (s.Item is ToolItem && s.Item is RepairableItem repairable)
                    {
                        if (repairable.DurabilityRate < 1.0f)
                        {
                            repairable.Durability = 100;
                        }
                    }
                });
            }
        }

        [ChatSubCommand(DefaultCommandName, "Adds item to inventory by name ID.", "getbyid", ChatAuthorizationLevel.Admin)]
        public static void GetItemByTypeName(User user, string itemTypeName)
        {
            Item item = Item.Get(itemTypeName);
            if (item != null)
            {
                user.Inventory.AddItem(item);
                user.MsgLocStr($"Item <color={VALUE_COLOR}>{item.Name}</color> have been added.");
            }
            else
            {
                user.MsgLocStr($"Item type name <color={VALUE_COLOR}>{itemTypeName}</color> does not exist in the game.");
            }
        }

        #endregion

        [ChatSubCommand(DefaultCommandName, "Lists all hidden items.", "showhidden", ChatAuthorizationLevel.Admin)]
        public static void ShowAllHiddenItems(User user)
        {
            StringBuilder sb = new();
            sb.Append(CreateTitleLine("Hidden Items"));

            Item.AllItems.Where(i => i.Hidden).ForEach(i =>
            {
                sb.Append($"{i.UILink()}\n");
            });
            sb.Append("\n");
            user.MsgLocStr(sb.ToString());
        }

        [ChatSubCommand(DefaultCommandName, "Finds the item in all storages or inventories.", "finditem", ChatAuthorizationLevel.Admin)]
        public static void FindItem(User user, string itemName)
        {
            StringBuilder sb = new();

            var searchItem = itemName.ClosestMatchingEntity(user);
            if (searchItem == null)
            {
                sb.Append($"Unknown item name <color={VALUE_COLOR}>{itemName}</color>. Did you enter the item name correctly?\n");
                user.MsgLocStr(sb.ToString());
                return;
            }
            int total = 0;
            int storageTotal = 0;
            int userTotal = 0;

            IEnumerable<StorageComponent> storages = WorldObjectUtil.AllObjsWithComponent<StorageComponent>();
            storages.ForEach(storage =>
            {
                ItemStack[] stacks = storage.Inventory.Stacks.Where(slot => slot.Item != null && slot.Item.GetType().IsEquivalentTo(searchItem.GetType())).ToArray();
                if (stacks.Length > 0)
                {
                    if (storageTotal == 0)
                    {
                        sb.Append(CreateSubTitleLine("Storage Inventory"));
                    }
                    int sum = SumItems(stacks);
                    sb.Append($"{storage.Parent.Owners.UILinkGeneric()} have <color={VALUE_COLOR}>{stacks.Length}x</color> stacks (<color={VALUE_COLOR}>{sum}</color>) in {storage.Parent.UILink()}\n");
                    storageTotal++;
                }
            });
            total += storageTotal;

            UserManager.Users.ForEach(u =>
            {
                ItemStack[] backpackStacks = u.Inventory.ToolbarBackpack.Stacks.Where(slot => slot.Item != null && slot.Item.GetType().IsEquivalentTo(searchItem.GetType())).ToArray();
                ItemStack[] carriedStacks = u.Inventory.Carried.Stacks.Where(slot => slot.Item != null && slot.Item.GetType().IsEquivalentTo(searchItem.GetType())).ToArray();

                if (backpackStacks.Length > 0 || carriedStacks.Length > 0)
                {
                    if (userTotal == 0)
                    {
                        sb.Append(CreateSubTitleLine("User Inventory"));
                    }
                    if (backpackStacks.Length > 0)
                    {
                        int sum = SumItems(backpackStacks);
                        sb.Append($"{u.UILink()} has <color={VALUE_COLOR}>{backpackStacks.Length}x</color> stacks (<color={VALUE_COLOR}>{sum}</color>) in backpack {searchItem.UILink()}\n");
                    }
                    if (carriedStacks.Length > 0)
                    {
                        int sum = SumItems(carriedStacks);
                        sb.Append($"{u.UILink()} carries <color={VALUE_COLOR}>{carriedStacks.Length}x</color> stacks (<color={VALUE_COLOR}>{sum}</color>) in hands {searchItem.UILink()}\n");
                    }
                    userTotal++;
                }
            });
            total += userTotal;

            if (total == 0)
            {
                sb.Append($"Nobody owns this item {searchItem.UILink()} in the world yet.\n");
            }
            user.SendInfoBox($"Find item: {searchItem.UILink()}", sb.ToString());
        }

        [ChatSubCommand(DefaultCommandName, "Number of items in the world.", "countof", ChatAuthorizationLevel.Admin)]
        public static void CountOfItem(User user, string itemNamesFormat)
        {
            string[] itemNames = itemNamesFormat.Split(';');

            StringBuilder sb = new();
            sb.Append(CreateTitleLine("Count Of"));

            foreach (string itemName in itemNames)
            {
                string msg = "";
                CountOfItemInternal(user, itemName, ref msg);
                sb.Append(msg);
            }
            user.MsgLocStr(sb.ToString());
        }

        [ChatSubCommand(DefaultCommandName, "Finds an object in the world. (does not support blocks)", "findobject", ChatAuthorizationLevel.Admin)]
        public static void FindObject(User user, string itemName)
        {
            StringBuilder sb = new();

            Item searchItem = itemName.ClosestMatchingEntity(user);
            if (searchItem == null)
            {
                sb.Append($"Unknown item name <color={VALUE_COLOR}>{itemName}</color>. Did you enter the object name correctly?\n");
                user.MsgLocStr(sb.ToString());
                return;
            }
            if (searchItem.GetType().IsSubclassOf(typeof(BlockItem)))
            {
                sb.Append("It is not possible to search for block items yet.\n");
                user.MsgLocStr(sb.ToString());
                return;
            }
            if (!(searchItem.GetType().IsSubclassOf(typeof(WorldObjectItem)) || searchItem.GetType().IsSubclassOf(typeof(BlockItem))))
            {
                sb.Append($"{searchItem.UILink()} is not a placeable object.\n");
                user.MsgLocStr(sb.ToString());
                return;
            }
            sb.Append(CreateSubTitleLine($"{searchItem.UILink()}"));

            WorldObject[] worldObjects = ServiceHolder<IWorldObjectManager>.Obj.All.Where(o => o?.CreatingItem?.Type == searchItem.Type).OrderBy(o => o.Owners).ToArray();
            if (worldObjects.Length > 0)
            {
                foreach (WorldObject obj in worldObjects)
                {
                    float playerDistance = Vector3.Distance(obj.Position, user.Position);
                    LocString distMeters = ShortLocs.Meters(playerDistance);

                    sb.Append($"{obj.Position.UILink()} = Owner: <color={VALUE_COLOR}>{obj.Creator?.Name ?? "-"}</color>, Distance: <color={VALUE_COLOR}>{distMeters}</color>, Marked: {obj?.DisplayName ?? obj?.Name}\n");
                }
            }
            else
            {
                sb.Append($"No {searchItem.UILink()} found in the world.\n");
            }
            user.SendInfoBox($"Find object: {searchItem.UILink()}", sb.ToString());
        }

        [ChatSubCommand(DefaultCommandName, "Finds the Ecko Statue in the world", "findecko", ChatAuthorizationLevel.Admin)]
        public static void FindEckoStatue(User user)
        {
            Item ecko = Item.Get<EckoStatueItem>();

            StringBuilder sb = new();
            sb.Append(CreateTitleLine($"Find {ecko.DisplayName}"));

            WorldObject[] worldObjects = ServiceHolder<IWorldObjectManager>.Obj.All.Where(o => o?.CreatingItem?.Type == ecko.Type).OrderBy(o => o.Owners).ToArray();

            if (worldObjects.Length > 0)
            {
                foreach (WorldObject obj in worldObjects)
                {
                    if (obj != null)
                    {
                        LocString name = obj?.DisplayName ?? new LocString(obj?.Name ?? "Ecko Statue");
                        sb.Append($"{obj?.Position.UILink()} ({obj?.Creator?.Name ?? "Game"}) with name: {name}\n");
                    }
                }
            }
            else
            {
                sb.Append("No Ecko has been found in the world.\n");
            }
            sb.Append("\n");
            user.MsgLocStr(sb.ToString());
        }

        [ChatSubCommand(DefaultCommandName, "Get user inventory", "getuserinv", ChatAuthorizationLevel.Admin)]
        public static void GetUserInventory(User user, User targetUser)
        {
            bool hasItems = false;
            StringBuilder sb = new();
            sb.Append(CreateTitleLine($"{user.UILink()} Inventory"));

            foreach (ItemStack stack in targetUser.Inventory.ToolbarBackpack.Stacks.Where(stack => stack.Quantity > 0))
            {
                sb.AppendLine($"{stack.Quantity}x {stack.Item.UILink()}");
                hasItems = true;
            }
            foreach (ItemStack stack in targetUser.Inventory.Carried.Stacks.Where(stack => stack.Quantity > 0))
            {
                sb.AppendLine($"Carries {stack.Quantity}x {stack.Item.UILink()}");
                hasItems = true;
            }
            if (!hasItems)
            {
                sb.Append($"No items.\n");
            }
            user.SendInfoBox($"User inventory: {targetUser.Name}", sb.ToString());
        }

        [ChatSubCommand(DefaultCommandName, "Add Meteor Shard to inventory", "getmeteorshard", ChatAuthorizationLevel.Admin)]
        public static void GetMeteorShard(User user)
        {
            user.Inventory.AddItem<MeteorShardItem>();
        }

        [ChatSubCommand(DefaultCommandName, "Add Whisperer Suit (3 items) to inventory", "getwhisperer", ChatAuthorizationLevel.Admin)]
        public static void GetWhispererSuit(User user)
        {
            user.Inventory.AddItem<WolfWhispererHatItem>();
            user.Inventory.AddItem<WolfWhispererMaskItem>();
            user.Inventory.AddItem<WolfWhispererShirtItem>();
        }

        [ChatSubCommand(DefaultCommandName, "Add Alpha Suit (3 items) to inventory", "getalpha", ChatAuthorizationLevel.Admin)]
        public static void GetAlphaSuit(User user)
        {
            user.Inventory.AddItem<AlphaCloakItem>();
            user.Inventory.AddItem<AlphaGogglesItem>();
            user.Inventory.AddItem<AlphaHatItem>();
        }

        [ChatSubCommand(DefaultCommandName, "List of all users, including offline ones", "allusers", ChatAuthorizationLevel.Admin)]
        public static void PrintAllUsers(User user)
        {
            PrintUsers(user, false);
        }

        [ChatSubCommand(DefaultCommandName, "List of online users", "onlineusers", ChatAuthorizationLevel.Admin)]
        public static void PrintOnlineUsers(User user)
        {
            PrintUsers(user, true);
        }

        [ChatSubCommand(DefaultCommandName, "Turns off all stations that produce pollution", "turnoffpollution", ChatAuthorizationLevel.Admin)]
        public static void TurnOffAllAirPollutionProducers(User user)
        {
            var pollutionsComponents = WorldObjectUtil.AllObjsWithComponent<AirPollutionComponent>();
            int count = 0;

            pollutionsComponents.Where(p => p.Parent.CreatingItem.RequiresComponent<OnOffComponent>()).ForEach(p =>
            {
                var onOff = p.Parent.Components.Where(c => c is OnOffComponent && c is not VehicleComponent).First() as OnOffComponent;
                if (onOff != null && onOff.On)
                {

                    onOff.On = false;
                    count++;
                }
            });
            NotificationManager.Msg(new LocString($"Turned off <color=orange>{count}</color> objects that produced CO2!"), Tools.GetOnlineUsers());
        }

        [ChatSubCommand(DefaultCommandName, "Removes ALL inactive players objects.", "removeinactive", ChatAuthorizationLevel.Admin)]
        public static void RemoveAll(User user, int days = 5)
        {
            HashSet<User> abandonedOwners = new();
            KeyValuePair<PlotPos, PropertyPlot>[] claims = PropertyManager.Data.PlotPosToProperty.Where(delegate (KeyValuePair<PlotPos, PropertyPlot> x)
            {
                User plotOwner = x.Value?.Owners?.OneUser();
                if (plotOwner != null && WorldTime.Seconds - plotOwner.LastOnlineTime > TimeUtil.DaysToSeconds(days))
                {
                    abandonedOwners.Add(plotOwner);
                    return true;
                }
                return false;
            }).ToArray();

            PropertyCommands.UnclaimAbandoned(user, days);

            var allObjects = WorldObjectUtil.AllObjsWithComponent<WorldObjectComponent>();
            allObjects.ForEach(p =>
            {
                var creator = p?.Parent?.Creator?.FirstUser();
                if (creator != null)
                {
                    if (abandonedOwners.Contains(creator))
                    {
                        p.Parent.Destroy();
                    }
                }
            });
        }

        public static int CountOfItemInternal(User user, string itemName, ref string msg)
        {
            StringBuilder sb = new();

            var searchItem = itemName.ClosestMatchingEntity(user);
            if (searchItem == null)
            {
                sb.Append($"Unknown item name <color={VALUE_COLOR}>{itemName}</color>. Did you enter the item name correctly?\n");
                msg = sb.ToString();
                return 0;
            }
            int total = 0;

            IEnumerable<StorageComponent> storages = WorldObjectUtil.AllObjsWithComponent<StorageComponent>();
            storages.ForEach(storage =>
            {
                ItemStack[] stacks = storage.Inventory.Stacks.Where(slot => slot.Item != null && slot.Item.GetType().IsEquivalentTo(searchItem.GetType())).ToArray();
                if (stacks.Length > 0)
                {
                    int sum = SumItems(stacks);
                    total += sum;
                }
            });

            UserManager.Users.ForEach(u =>
            {
                ItemStack[] backpackStacks = u.Inventory.ToolbarBackpack.Stacks.Where(slot => slot.Item != null && slot.Item.GetType().IsEquivalentTo(searchItem.GetType())).ToArray();
                ItemStack[] carriedStacks = u.Inventory.Carried.Stacks.Where(slot => slot.Item != null && slot.Item.GetType().IsEquivalentTo(searchItem.GetType())).ToArray();

                if (backpackStacks.Length > 0 || carriedStacks.Length > 0)
                {
                    if (backpackStacks.Length > 0)
                    {
                        int sum = SumItems(backpackStacks);
                        total += sum;
                    }
                    if (carriedStacks.Length > 0)
                    {
                        int sum = SumItems(carriedStacks);
                        total += sum;
                    }
                }
            });

            if (total > 0)
            {
                sb.Append($"There are a total of <color={VALUE_COLOR}>{total}</color> {searchItem.UILink()} in the world!");
            }
            else
            {
                sb.Append($"Nobody owns this item {searchItem.UILink()} in the world yet.\n");
            }
            msg = sb.ToString();
            return total;
        }

        private static void PrintUsers(User user, bool onlineOnly)
        {
            StringBuilder sb = new();
            sb.Append(CreateTitleLine(onlineOnly ? "Online Users" : "Users"));

            List<UserData> usersStruct = new();
            int longestNameLength = 0;
            UserManager.Users.ForEach(u =>
            {
                if (u != null && (onlineOnly && u.IsOnline || !onlineOnly))
                {
                    usersStruct.Add(new UserData
                    {
                        user = u,
                        skillRate = (float)Math.Round(u?.SkillRateSafe() ?? 0, 1),
                        stars = u?.TotalStarsEarned() ?? 0
                    });
                    longestNameLength = Math.Max(longestNameLength, u.RepUILink().Length);
                }
            });
            usersStruct.OrderBy(p => p.user.IsOnline).ThenBy(p => p.stars).ThenBy(p => p.skillRate).ThenBy(p => p.user.Name).ForEach(u =>
            {
                string strLine = PrepareUserInfoLine(u, longestNameLength);
                sb.Append(strLine);
                sb.Append("\n");
            });
            user.SendInfoBox($"Users: {usersStruct.Count}", sb.ToString());
        }

        private static string PrepareUserInfoLine(UserData data, int longestNameLength)
        {
            StringBuilder sb = new();
            string valColor = "#DAB287";

            User u = data.user;
            double xpPercent = u.NextLevelPercentSafe();
            string repColor = u?.ReputationSafe() < 0 ? "#EE6666" : "#66EE66";

            sb.Append($"<color=#FFFFFF>{u?.RepUILink().PadRight(longestNameLength)}</color> = ");
            //sb.Append($"IP: <color={valColor}>{u?.IpAddress()?.ToString() ?? "NULL"}</color>, ");
            sb.Append($"Steam: <color={valColor}>{u?.SteamIdSafe() ?? "NULL"}</color>, ");
            sb.Append($"Slg: <color={valColor}>{u?.SlgIdSafe() ?? "NULL"}</color>, ");
            sb.Append($"Reputation: <color={repColor}>{u?.ReputationSafe()}</color>, ");
            sb.Append($"Housing: <color={valColor}>{Math.Round(u?.HousingValueSafe() ?? 0, 1)}</color>, ");
            sb.Append($"Food: <color={valColor}>{Math.Round(u?.StomachSkillRateSafe() ?? 0, 1)}</color>, ");
            sb.Append($"XP: <color={valColor}>{xpPercent}%</color> (<color={valColor}>{Math.Round(u?.XpSafe() ?? 0)}/{u?.NextStarCostSafe() ?? 0}</color>), ");
            sb.Append($"Rate: <color={valColor}>{data.skillRate}</color>, ");
            sb.Append($"Stars/Free: <color={valColor}>{data.stars}/{u?.StarsAvailableSafe() ?? 0}</color>, ");
            sb.Append($"Prof/Spec: <color={valColor}>{u?.ProfessionCountSafe() ?? 0}/{u?.SpecialtyCountSafe() ?? 0}</color>, ");
            sb.Append($"Active: <color={valColor}>{u?.IsActiveSafe() ?? false}</color>, ");
            sb.Append($"Play time: <color={valColor}>{u?.TotalPlayTimeFormatted()}</color>");

            return sb.ToString();
        }

        private struct UserData
        {
            public User user;
            public float skillRate;
            public int stars;
        }
    }
}
