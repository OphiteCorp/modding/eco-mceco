﻿using Eco.Core;
using Eco.Core.Plugins;
using Eco.Core.Utils;
using McEco.Lib;

namespace McEco.Commands
{
    public class InitPlugin : AbstractInitConfigurablePlugin
    {
        public const string DefaultModuleName = "Commands";

        private readonly PluginConfig<Config> config = new(DefaultCategory + DefaultModuleName);

        public static InitPlugin Obj => PluginManager.GetPlugin<InitPlugin>();

        public Config Config => config.Config;

        public override IPluginConfig PluginConfig => config;

        public InitPlugin()
        {
            ModuleName = DefaultModuleName;
        }

        public override object GetEditObject()
        {
            return config.Config;
        }

        public override void OnEditObjectChanged(object o, string param)
        {
            config.SaveAsync().Wait();
        }

        protected override void StartUp(TimedTask timed)
        {
        }
    }
}