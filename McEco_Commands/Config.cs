﻿using System.ComponentModel;

namespace McEco.Commands
{
    public class Config
    {
        [DisplayName("Better unstuck delay")]
        [Description("Time delay for better unstuck in seconds.")]
        public int BetterUnstuckDelay { get; set; } = 60;
    }
}
