﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Core.Items;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Modules;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;
    using Eco.Gameplay.Housing.PropertyValues;
    using Eco.Gameplay.Civics.Objects;
    using Eco.Gameplay.Settlements;
    using Eco.Gameplay.Systems.NewTooltip;
    using Eco.Core.Controller;
    using static Eco.Gameplay.Housing.PropertyValues.HomeFurnishingValue;
    using System.Reflection;

    [Serialized]
    //required components stolen from "steam engine.cs"
    //[RequireComponent(typeof(AirPollutionComponent))]
    //[RequireComponent(typeof(ChimneyComponent))]
    [RequireComponent(typeof(LiquidProducerComponent))]
    [RequireComponent(typeof(AttachmentComponent))]
    [RequireComponent(typeof(OnOffComponent))]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(FuelSupplyComponent))]
    [RequireComponent(typeof(FuelConsumptionComponent))]
    //[RequireComponent(typeof(PowerGridComponent))]
    //[RequireComponent(typeof(PowerGeneratorComponent))]
    [RequireComponent(typeof(HousingComponent))]
    [RequireComponent(typeof(SolidAttachedSurfaceRequirementComponent))]
    [RequireComponent(typeof(LiquidConsumerComponent))]
    //[PowerGenerator(typeof(ElectricPower))] 

    public partial class ReactorObject : WorldObject, IRepresentsItem
    {
        public virtual Type RepresentedItemType => typeof(ReactorItem);
        public override LocString DisplayName => Localizer.DoStr("Nuclear Reactor");
        public override TableTextureMode TableTexture => TableTextureMode.Metal;

        private static string[] fuelTagList = new string[] { "Uranium Fuel" };

        protected override void Initialize()
        {
            this.GetComponent<FuelSupplyComponent>().Initialize(2, fuelTagList);
            this.GetComponent<FuelConsumptionComponent>().Initialize(baseConsumption);
            this.GetComponent<HousingComponent>().HomeValue = SteamEngineItem.homeValue;
            this.GetComponent<LiquidProducerComponent>().Setup(typeof(SteamItem), baseSteam, this.GetOccupancyType(BlockOccupancyType.OutputPort));
            this.GetComponent<LiquidConsumerComponent>().Setup(typeof(WaterItem), 2f, this.GetOccupancyType(BlockOccupancyType.WaterInputPort), 0.9f);

            //this.GetComponent<LiquidConverterComponent>().Setup(Item.Get("WaterItem").Type, Item.Get("SteamItem").Type, this.NamedOccupancyOffset("InputPort"), this.NamedOccupancyOffset("OutputPort"), 1f, 1f);
        }

        private DateTime lastT = DateTime.Now.AddSeconds(-1);
        private readonly Random random = new();
        private const int baseConsumption = 1000;
        private const float baseSteam = 1f;

        public override void Tick()
        {
            if (lastT < DateTime.Now.AddSeconds(-1))
            {
                var cons = this.GetComponent<FuelConsumptionComponent>();
                var liq = this.GetComponent<LiquidProducerComponent>();
                var liqField = liq.GetType().GetField("constantProductionRate", BindingFlags.NonPublic | BindingFlags.Instance);

                if (this.GetComponent<OnOffComponent>().On)
                {
                    cons.JoulesPerSecond = random.Range(baseConsumption - 50, baseConsumption + 25);
                    liqField?.SetValue(liq, random.Range(baseSteam - 0.1f, baseSteam + 0.25f));
                }
                else
                {
                    cons.JoulesPerSecond = baseConsumption;
                    liqField?.SetValue(liq, baseSteam);
                }
                lastT = DateTime.Now;
            }
            base.Tick();
        }
    }

    [Serialized]
    [LocDisplayName("Nuclear Reactor")]
    [Ecopedia("Crafted Objects", "Power Generation", subPageName: "Nuclear Reactor Item")]
    [Weight(100)]
    public partial class ReactorItem :
        WorldObjectItem<ReactorObject>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr($"We're all pretty sure the engineer just took a propane tank, stuffed it full of 'engineering' and pocketed the rest of the materials. Although he's promised us this baby can produce {Text.Info(10000)}w in steam power for a turbine."); } }


        public override DirectionAxisFlags RequiresSurfaceOnSides { get; } = 0
                    | DirectionAxisFlags.Down
                ;
        public override HomeFurnishingValue HomeValue => homeValue;
        public static readonly HomeFurnishingValue homeValue = new HomeFurnishingValue()
        {
            Category = HousingConfig.GetRoomCategory("Industrial"),
            TypeForRoomLimit = Localizer.DoStr(""),
        };

        [Tooltip(7)] private LocString PowerConsumptionTooltip => Localizer.Do($"Consumes: {Text.Info(1000)}w of {new ElectricPower().Name} power from fuel");
        [Tooltip(8)] private LocString PowerProductionTooltip => Localizer.Do($"Produces: {Text.Info(10000)}w of steam power ");

        static ReactorItem()
        {
            WorldObject.AddOccupancy<ReactorObject>(new List<BlockOccupancy>(){
            new BlockOccupancy(new Vector3i(0, 0, 0)),
            //new BlockOccupancy(new Vector3i(1, 0, 0)),
            new BlockOccupancy(new Vector3i(1, 0, -1)),
            //new BlockOccupancy(new Vector3i(0, 0, -1)),

            new BlockOccupancy(new Vector3i(0, 1, 0)),
            new BlockOccupancy(new Vector3i(1, 1, 0)),
            new BlockOccupancy(new Vector3i(1, 1, -1)),
            new BlockOccupancy(new Vector3i(0, 1, -1)),

            new BlockOccupancy(new Vector3i(0, 2, 0)),
            new BlockOccupancy(new Vector3i(1, 2, 0)),
            new BlockOccupancy(new Vector3i(1, 2, -1)),
            new BlockOccupancy(new Vector3i(0, 2, -1)),

            new BlockOccupancy(new Vector3i(1, 0, 0), typeof(PipeSlotBlock), new Quaternion(0f, -0.7071068f, 0f, -0.7071068f), BlockOccupancyType.WaterInputPort),
            new BlockOccupancy(new Vector3i(0, 0, -1), typeof(PipeSlotBlock), new Quaternion(0f, -0.7071068f, 0f, 0.7071068f), BlockOccupancyType.OutputPort),
            });

        }
    }

    public partial class ReactorRecipe :
        RecipeFamily
    {
        public ReactorRecipe()
        {
            this.Recipes = new List<Recipe>
            {
                new Recipe(
                    "Reactor",
                    Localizer.DoStr("Reactor"),
                    new IngredientElement[]
                    {
                    new IngredientElement(typeof(RadiatorItem), 5*2),
                    new IngredientElement(typeof(ServoItem), 10*2),
                    new IngredientElement(typeof(RivetItem), 20*2),
                    new IngredientElement(typeof(AdvancedCircuitItem), 10*2),
                    new IngredientElement(typeof(SteelPipeItem), 30*2),
                    new IngredientElement(typeof(SteelPlateItem), 30*2),
                    new IngredientElement(typeof(CementItem), 50*2),
                    },
                    new CraftingElement[]
                    {
                    new CraftingElement<ReactorItem>(1),
                    }
                )
            };

            this.ExperienceOnCraft = 20f;
            this.LaborInCalories = CreateLaborInCaloriesValue(3000);
            this.CraftMinutes = CreateCraftTimeValue(60f);
            this.Initialize(Localizer.DoStr("Reactor"), typeof(ReactorRecipe));

            CraftingComponent.AddRecipe(typeof(RoboticAssemblyLineObject), this);
        }
    }
}