﻿namespace Eco.Mods.TechTree
{
        using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Core.Items;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;

    [RequiresSkill(typeof(MiningSkill), 7)] 
    public partial class FuelCellRecipe :
        RecipeFamily
    {
        public FuelCellRecipe()
        {
            this.Recipes = new List<Recipe>
            {
                new Recipe(
                    "Copper Ore Uranium Extraction",
                    Localizer.DoStr("Copper Ore Uranium Extraction"),
                    new IngredientElement[]
                    {
                    new IngredientElement(typeof(CrushedCopperOreItem), 100 * 2),
                    new IngredientElement(typeof(SteelPlateItem), 2),
                    },
                    new CraftingElement[]
                    {
                    new CraftingElement<FuelCellItem>(1),  
                    new CraftingElement<CopperConcentrateItem>(20),
                    new CraftingElement<WetTailingsItem>(30),
                    }
                )
            };


            this.ExperienceOnCraft = 20;
            this.LaborInCalories = CreateLaborInCaloriesValue(10000, typeof(MiningSkill)); 
            this.CraftMinutes = CreateCraftTimeValue(120.0f);
            this.Initialize(Localizer.DoStr("Copper Ore Uranium Extraction"), typeof(FuelCellRecipe));

            CraftingComponent.AddRecipe(typeof(FrothFloatationCellObject), this);
        }
    }

    [Serialized]
    [LocDisplayName("Fuel Cell")]
    [Weight(100)]      
    [Fuel(8000000)][Tag("Fuel")]          
    [Tag("Currency")][Currency]              
    //[Ecopedia("Items", "Tools", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]                                                                           
    [Tag("Uranium Fuel", 1)]                                 
    public partial class FuelCellItem :
    Item                                    
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr("A highly shielded cell of uranium which contains a very large amount of energy"); } }
    }
}
