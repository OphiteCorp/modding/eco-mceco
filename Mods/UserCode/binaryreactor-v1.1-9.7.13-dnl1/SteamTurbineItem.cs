﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Core.Items;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Modules;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;
    using Eco.Gameplay.Housing.PropertyValues;
    using Eco.Gameplay.Civics.Objects;
    using Eco.Gameplay.Settlements;
    using Eco.Gameplay.Systems.NewTooltip;
    using Eco.Core.Controller;
    using static Eco.Gameplay.Housing.PropertyValues.HomeFurnishingValue;
    using System.Linq;

    [Serialized]
    //[RequireComponent(typeof(LiquidProducerComponent))]
    [RequireComponent(typeof(AttachmentComponent))]
    [RequireComponent(typeof(OnOffComponent))]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(PowerGridComponent))]
    [RequireComponent(typeof(PowerGeneratorComponent))]
    [RequireComponent(typeof(HousingComponent))]
    [RequireComponent(typeof(SolidAttachedSurfaceRequirementComponent))]
    [RequireComponent(typeof(LiquidConsumerComponent))]
    [PowerGenerator(typeof(ElectricPower))]

    public partial class SteamTurbineObject : WorldObject, IRepresentsItem
    {
        public virtual Type RepresentedItemType => typeof(SteamTurbineItem);
        public override LocString DisplayName => Localizer.DoStr("Steam Turbine");
        public override TableTextureMode TableTexture => TableTextureMode.Metal;

        protected override void Initialize()
        {
            this.GetComponent<PowerGridComponent>().Initialize(30, new ElectricPower());
            this.GetComponent<PowerGeneratorComponent>().Initialize(basePower);
            this.GetComponent<LiquidConsumerComponent>().Setup(typeof(SteamItem), 0.5f, this.GetOccupancyType(BlockOccupancyType.WaterInputPort), 0.9f);


            var c = this.GetComponent<PowerGeneratorComponent>();
            c.Initialize(20);

            //this.GetComponent<LiquidConverterComponent>().Setup(Item.Get("SteamItem").Type, Item.Get("WaterItem").Type, this.NamedOccupancyOffset("InputPort"), this.NamedOccupancyOffset("OutputPort"), 1.1f, 0.9f);
        }

        private DateTime lastT = DateTime.Now.AddSeconds(-1);
        private readonly Random random = new();
        private const int basePower = 10000;

        public override void Tick()
        {
            if (lastT < DateTime.Now.AddSeconds(-1))
            {
                var comp = this.GetComponent<PowerGeneratorComponent>();

                if (this.GetComponent<OnOffComponent>().On)
                {
                    comp.UpdateJoulesPerSecond(random.Range(basePower - 50, basePower + 200));
                }
                else
                {
                    comp.UpdateJoulesPerSecond(basePower);
                }
                lastT = DateTime.Now;
            }
            base.Tick();
        }
    }

    [Serialized]
    [LocDisplayName("Steam Turbine")]
    //[Ecopedia("Crafted Objects", "Power Generation", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
    [Weight(100)]
    public partial class SteamTurbineItem :
        WorldObjectItem<SteamTurbineObject>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr($"A complicated piece of machinery made to turn steam power into {new ElectricPower().Name}. This unit can handle up to {Text.Info(10000)}w, the amount produced by a small nuclear reactor."); } }


        public override DirectionAxisFlags RequiresSurfaceOnSides { get; } = 0
                    | DirectionAxisFlags.Down
                ;
        public override HomeFurnishingValue HomeValue => homeValue;
        public static readonly HomeFurnishingValue homeValue = new HomeFurnishingValue()
        {
            Category = HousingConfig.GetRoomCategory("Industrial"),
            TypeForRoomLimit = Localizer.DoStr(""),
        };

        [Tooltip(7)] private LocString PowerConsumptionTooltip => Localizer.Do($"Consumes: {Text.Info(10000)}w of steam power");
        [Tooltip(8)] private LocString PowerProductionTooltip => Localizer.Do($"Produces: {Text.Info(10000)}w of {new ElectricPower().Name} power (electricity)");

        static SteamTurbineItem()
        {
            WorldObject.AddOccupancy<SteamTurbineObject>(new List<BlockOccupancy>(){
            //new BlockOccupancy(new Vector3i(0, 0, 0)),
            new BlockOccupancy(new Vector3i(0, 0, -1)),
            new BlockOccupancy(new Vector3i(1, 0, 0)),
            new BlockOccupancy(new Vector3i(1, 0, -1)),
            new BlockOccupancy(new Vector3i(2, 0, 0)),
            new BlockOccupancy(new Vector3i(2, 0, -1)),
            //new BlockOccupancy(new Vector3i(3, 0, 0)),
            new BlockOccupancy(new Vector3i(3, 0, -1)),

            new BlockOccupancy(new Vector3i(0, 1, 0)),
            new BlockOccupancy(new Vector3i(0, 1, -1)),
            new BlockOccupancy(new Vector3i(1, 1, 0)),
            new BlockOccupancy(new Vector3i(1, 1, -1)),
            new BlockOccupancy(new Vector3i(2, 1, 0)),
            new BlockOccupancy(new Vector3i(2, 1, -1)),
            new BlockOccupancy(new Vector3i(3, 1, 0)),
            new BlockOccupancy(new Vector3i(3, 1, -1)),

            new BlockOccupancy(new Vector3i(0, 0, 0), typeof(PipeSlotBlock), new Quaternion(0f, 0f, 0f, 1f), BlockOccupancyType.WaterInputPort),
            new BlockOccupancy(new Vector3i(3, 0, 0), typeof(PipeSlotBlock), new Quaternion(0f, 0f, 0f, 1f), BlockOccupancyType.OutputPort),
            });

        }
    }

    public partial class SteamTurbineRecipe :
        RecipeFamily
    {
        public SteamTurbineRecipe()
        {
            this.Recipes = new List<Recipe>
            {
                new Recipe(
                    "SteamTurbine",
                    Localizer.DoStr("SteamTurbine"),
                    new IngredientElement[]
                    {
                    new IngredientElement(typeof(SteelAxleItem), 5*2),
                    new IngredientElement(typeof(SteelGearboxItem), 5*2),
                    new IngredientElement(typeof(SteelPipeItem), 10*2),
                    new IngredientElement(typeof(RadiatorItem), 10*2),
                    new IngredientElement(typeof(AdvancedCircuitItem), 10*2),
                    new IngredientElement(typeof(SteelPlateItem), 30*2),
                    new IngredientElement(typeof(CementItem), 50*2),
                    },
                    new CraftingElement[]
                    {
                    new CraftingElement<SteamTurbineItem>(1),
                    }
                )
            };

            this.ExperienceOnCraft = 20f;
            this.LaborInCalories = CreateLaborInCaloriesValue(3000);
            this.CraftMinutes = CreateCraftTimeValue(60f);
            this.Initialize(Localizer.DoStr("SteamTurbine"), typeof(SteamTurbineRecipe));

            CraftingComponent.AddRecipe(typeof(RoboticAssemblyLineObject), this);
        }
    }
}