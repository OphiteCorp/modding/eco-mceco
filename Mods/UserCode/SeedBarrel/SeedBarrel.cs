namespace Eco.Mods.TechTree
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Core.Items;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Modules;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;

    [Serialized]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(PublicStorageComponent))]
    public partial class SeedBarrelObject : WorldObject, IRepresentsItem
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Incredible Barrel"); } }

        public override TableTextureMode TableTexture => TableTextureMode.Wood;

        public virtual Type RepresentedItemType { get { return typeof(SeedBarrelItem); } }

        protected override void Initialize()
        {
            var storage = GetComponent<PublicStorageComponent>();
            storage.Initialize(200);
            storage.Storage.AddInvRestriction(new StackLimitRestriction(int.MaxValue, true));
            storage.ShelfLifeMultiplier = short.MaxValue;
            GetComponent<LinkComponent>().Initialize(30);
        }
    }

    [Serialized]
    //[Category("Hidden")]
    [LocDisplayName("Incredible Barrel")]
    [MaxStackSize(200)]
    public partial class SeedBarrelItem : WorldObjectItem<SeedBarrelObject>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr("A barrel to store lots of items."); } }

        static SeedBarrelItem()
        {
            WorldObject.AddOccupancy<SeedBarrelObject>(new List<BlockOccupancy>(){
                new BlockOccupancy(new Vector3i(0, 0, 0)),
                new BlockOccupancy(new Vector3i(0, 1, 0)),
            });
        }
    }

    [RequiresSkill(typeof(FarmingSkill), 7)]
    public partial class SeedBarrelRecipe : RecipeFamily
    {
        public SeedBarrelRecipe()
        {
            var product = new Recipe(
                "SeedBarrel",
                Localizer.DoStr("Incredible Barrel"),
                new IngredientElement[]
                {
                   new IngredientElement(typeof(MeteorShardItem), 1),
                   new IngredientElement(typeof(EcoylentItem), 1)
                },
               new CraftingElement<SeedBarrelItem>()
            );
            Recipes = new List<Recipe> { product };
            LaborInCalories = CreateLaborInCaloriesValue(100);
            CraftMinutes = CreateCraftTimeValue(3);
            Initialize(Localizer.DoStr("Incredible Barrel"), typeof(SeedBarrelRecipe));
            CraftingComponent.AddRecipe(typeof(FarmersTableObject), this);
        }
    }
}