﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Items;
    using Eco.Shared.Localization;
    
    public class WashScooterRecipe : RecipeFamily
    {
        public WashScooterRecipe()
        {
            this.Recipes = new List<Recipe>
            {
                new Recipe(
                    "Wash Scooter Colored",
                    Localizer.DoStr("Wash Scooter Colored"),
                    new IngredientElement[]
                    {
                        new IngredientElement("ColoredScooter", 1, true),
                        new IngredientElement(typeof(PlantFibersItem), 8, typeof(BasicEngineeringSkill), typeof(BasicEngineeringLavishResourcesTalent)),
                    },
                    new CraftingElement<ScooterItem>()
                )
            };
            this.ExperienceOnCraft = 0.05f;
            this.LaborInCalories = CreateLaborInCaloriesValue(50, typeof(BasicEngineeringSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(WashScooterRecipe), 1, typeof(BasicEngineeringSkill));

            this.Initialize(Localizer.DoStr("Wash Scooter Colored"), typeof(WashScooterRecipe));
            CraftingComponent.AddRecipe(typeof(PrimitivePaintingTableObject), this);
        }
    }
}
