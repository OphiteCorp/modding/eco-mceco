namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Core.Items;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Items;
    using Eco.Core.Controller;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.NewTooltip;
    using Eco.Gameplay.Systems.Tooltip;

    [Serialized]
    [LocDisplayName("Scooter")]
    [Weight(5000)]
    [Ecopedia("CavRnMods", "Scooter", true)]
    [Tag("ColoredScooter")]
    public partial class ScooterItem : WorldObjectItem<ScooterObject>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr("A scooter for fast travel."); } }
        [Serialized, SyncToView, TooltipChildren, NewTooltipChildren(CacheAs.Disabled)] public object PersistentData { get; set; }
    }

    [RequiresSkill(typeof(BasicEngineeringSkill), 2)]
    public class ScooterRecipe : RecipeFamily
    {
        public ScooterRecipe()
        {
            this.Recipes = new List<Recipe>
            {
                new Recipe(
                    "Scooter",
                    Localizer.DoStr("Scooter"),
                    new IngredientElement[]
                    {
                        new IngredientElement(typeof(ClothItem), 2, typeof(BasicEngineeringSkill), typeof(BasicEngineeringLavishResourcesTalent)),
                        new IngredientElement(typeof(IronBarItem), 4, typeof(BasicEngineeringSkill), typeof(BasicEngineeringLavishResourcesTalent)),
                        new IngredientElement(typeof(IronWheelItem), 2, true),
                    },
                    new CraftingElement<ScooterItem>()
                )
            };
            this.ExperienceOnCraft = 5f;
            this.LaborInCalories = CreateLaborInCaloriesValue(300, typeof(BasicEngineeringSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(ScooterRecipe), 3, typeof(BasicEngineeringSkill));

            this.Initialize(Localizer.DoStr("Scooter"), typeof(ScooterRecipe));
            CraftingComponent.AddRecipe(typeof(WainwrightTableObject), this);
        }
    }

    [Serialized]
    [RequireComponent(typeof(StandaloneAuthComponent))]
    [RequireComponent(typeof(VehicleComponent))]
    [RequireComponent(typeof(CustomTextComponent))]
    public partial class ScooterObject : PhysicsWorldObject, IRepresentsItem
    {
        static ScooterObject()
        {
            WorldObject.AddOccupancy<ScooterObject>(new List<BlockOccupancy>(0));
        }

        public override TableTextureMode TableTexture => TableTextureMode.Metal;
        public override LocString DisplayName { get { return Localizer.DoStr("Scooter"); } }
        public Type RepresentedItemType { get { return typeof(ScooterItem); } }

        private ScooterObject() { }

        protected override void Initialize()
        {
            base.Initialize();

            this.GetComponent<CustomTextComponent>().Initialize(200);
            this.GetComponent<VehicleComponent>().Initialize(20, 1.6f, 1);
            this.GetComponent<VehicleComponent>().HumanPowered(0.125f);
        }
    }
}
