namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Core.Items;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Items;
    using Eco.Core.Controller;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.NewTooltip;
    using Eco.Gameplay.Systems.Tooltip;

    [Serialized]
    [LocDisplayName("Scooter White")]
    [Weight(5000)]
    [Ecopedia("CavRnMods", "Scooter", true)]
    [Tag("ColoredScooter")]
    public partial class ScooterWhiteItem : WorldObjectItem<ScooterWhiteObject>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr("A White scooter for fast travel."); } }
        [Serialized, SyncToView, TooltipChildren, NewTooltipChildren(CacheAs.Disabled)] public object PersistentData { get; set; }
    }

    public class PaintScooterWhiteRecipe : RecipeFamily
    {
        public PaintScooterWhiteRecipe()
        {
            this.Recipes = new List<Recipe>
            {
                new Recipe(
                    "Paint Scooter White",
                    Localizer.DoStr("Paint Scooter White"),
                    new IngredientElement[]
                    {
                        new IngredientElement(typeof(ScooterItem), 1, true),
                        new IngredientElement(typeof(LimestoneItem), 7, typeof(BasicEngineeringSkill), typeof(BasicEngineeringLavishResourcesTalent)),
                    },
                    new CraftingElement<ScooterWhiteItem>()
                )
            };
            this.ExperienceOnCraft = 0.05f;
            this.LaborInCalories = CreateLaborInCaloriesValue(100, typeof(BasicEngineeringSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(PaintScooterWhiteRecipe), 2.5f, typeof(BasicEngineeringSkill));

            this.Initialize(Localizer.DoStr("Paint Scooter White"), typeof(PaintScooterWhiteRecipe));
            CraftingComponent.AddRecipe(typeof(PrimitivePaintingTableObject), this);
        }
    }

    [Serialized]
    [RequireComponent(typeof(StandaloneAuthComponent))]
    [RequireComponent(typeof(VehicleComponent))]
    [RequireComponent(typeof(CustomTextComponent))]
    public partial class ScooterWhiteObject : PhysicsWorldObject, IRepresentsItem
    {
        static ScooterWhiteObject()
        {
            WorldObject.AddOccupancy<ScooterWhiteObject>(new List<BlockOccupancy>(0));
        }

        public override TableTextureMode TableTexture => TableTextureMode.Metal;
        public override LocString DisplayName { get { return Localizer.DoStr("Scooter White"); } }
        public Type RepresentedItemType { get { return typeof(ScooterWhiteItem); } }

        private ScooterWhiteObject() { }

        protected override void Initialize()
        {
            base.Initialize();

            this.GetComponent<CustomTextComponent>().Initialize(200);
            this.GetComponent<VehicleComponent>().Initialize(20, 1.6f, 1);
            this.GetComponent<VehicleComponent>().HumanPowered(0.125f);
        }
    }
}
