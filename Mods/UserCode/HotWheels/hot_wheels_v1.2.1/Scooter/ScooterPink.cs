namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Core.Items;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Items;
    using Eco.Core.Controller;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.NewTooltip;
    using Eco.Gameplay.Systems.Tooltip;

    [Serialized]
    [LocDisplayName("Scooter Pink")]
    [Weight(5000)]
    [Ecopedia("CavRnMods", "Scooter", true)]
    [Tag("ColoredScooter")]
    public partial class ScooterPinkItem : WorldObjectItem<ScooterPinkObject>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr("A Pink scooter for fast travel."); } }
        [Serialized, SyncToView, TooltipChildren, NewTooltipChildren(CacheAs.Disabled)] public object PersistentData { get; set; }
    }

    public class PaintScooterPinkRecipe : RecipeFamily
    {
        public PaintScooterPinkRecipe()
        {
            this.Recipes = new List<Recipe>
            {
                new Recipe(
                    "Paint Scooter Pink",
                    Localizer.DoStr("Paint Scooter Pink"),
                    new IngredientElement[]
                    {
                        new IngredientElement(typeof(ScooterItem), 1, true),
                        new IngredientElement(typeof(BeetItem), 5, typeof(BasicEngineeringSkill), typeof(BasicEngineeringLavishResourcesTalent)),
                        new IngredientElement(typeof(LimestoneItem), 3, typeof(BasicEngineeringSkill), typeof(BasicEngineeringLavishResourcesTalent)),
                    },
                    new CraftingElement<ScooterPinkItem>()
                )
            };
            this.ExperienceOnCraft = 0.05f;
            this.LaborInCalories = CreateLaborInCaloriesValue(100, typeof(BasicEngineeringSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(PaintScooterPinkRecipe), 2.5f, typeof(BasicEngineeringSkill));

            this.Initialize(Localizer.DoStr("Paint Scooter Pink"), typeof(PaintScooterPinkRecipe));
            CraftingComponent.AddRecipe(typeof(PrimitivePaintingTableObject), this);
        }
    }

    [Serialized]
    [RequireComponent(typeof(StandaloneAuthComponent))]
    [RequireComponent(typeof(VehicleComponent))]
    [RequireComponent(typeof(CustomTextComponent))]
    public partial class ScooterPinkObject : PhysicsWorldObject, IRepresentsItem
    {
        static ScooterPinkObject()
        {
            WorldObject.AddOccupancy<ScooterPinkObject>(new List<BlockOccupancy>(0));
        }

        public override TableTextureMode TableTexture => TableTextureMode.Metal;
        public override LocString DisplayName { get { return Localizer.DoStr("Scooter Pink"); } }
        public Type RepresentedItemType { get { return typeof(ScooterPinkItem); } }

        private ScooterPinkObject() { }

        protected override void Initialize()
        {
            base.Initialize();

            this.GetComponent<CustomTextComponent>().Initialize(200);
            this.GetComponent<VehicleComponent>().Initialize(20, 1.6f, 1);
            this.GetComponent<VehicleComponent>().HumanPowered(0.125f);
        }
    }
}
