﻿using Eco.Gameplay.Players;
using Eco.Shared.Networking;
using Eco.Shared.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.Json;

using User = Eco.Gameplay.Players.User;

namespace McEco.Lib.Helper
{
    public static class Tools
    {
        public static List<User> GetOnlineUsers()
        {
            return UserManager.Users.Where(p => p.IsOnline).OrderBy(p => p.Name).ToList();
        }

        public static string? GetAbsoluteFilePath(GameDir dir, string fileName)
        {
            string absolutePath = Path.Combine(Directory.GetCurrentDirectory(), dir.ToString(), AbstractInitPlugin.DefaultCategory, fileName);
            string? rootDir = Path.GetDirectoryName(absolutePath);

            if (rootDir != null && !Directory.Exists(rootDir))
            {
                Directory.CreateDirectory(rootDir);
            }
            return Directory.Exists(rootDir) ? absolutePath : null;
        }

        public static T? ImportConfig<T>(string configFile)
        {
            string? filePath = GetAbsoluteFilePath(GameDir.Configs, configFile);

            if (filePath != null && File.Exists(filePath))
            {
                string json = File.ReadAllText(filePath);
                return JsonSerializer.Deserialize<T>(json, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }
            return default;
        }

        public static void ShowInfoBox(User user, string title, string message)
        {
            BSONObject bson = BSONObject.New;
            bson["title"] = title;
            bson["text"] = message;
            bson["category"] = Guid.NewGuid().ToString();

            user.Client.RPC("OpenUI", user.Client, "InfoPanel", bson);
        }

        /// <summary>
        /// EXAMPLE
        /// 
        /// var setter = GetSetterForProperty<Application, float>(x => x.Value);
        /// setter?.Invoke(app, 5000f);
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static Action<T, TValue>? GetSetterForProperty<T, TValue>(Expression<Func<T, TValue>> selector) where T : class
        {
            Expression expression = selector.Body;
            PropertyInfo? propertyInfo = (expression.NodeType == ExpressionType.MemberAccess) ? (PropertyInfo)((MemberExpression)expression).Member : null;
            return (propertyInfo is not null) ? GetPropertySetter(propertyInfo) : null;

            static Action<T, TValue> GetPropertySetter(PropertyInfo prop)
            {
                MethodInfo? setter = prop.GetSetMethod(nonPublic: true);
                if (setter is not null)
                {
                    return (obj, value) => setter.Invoke(obj, new object?[] { value });
                }
                BindingFlags declaredOnlyLookup = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
                var backingField = prop.DeclaringType?.GetField($"<{prop.Name}>k__BackingField", declaredOnlyLookup);
                if (backingField is null)
                {
                    throw new InvalidOperationException($"Could not find a way to set {prop.DeclaringType?.FullName}.{prop.Name}. Try adding a private setter.");
                }
                return (obj, value) => backingField.SetValue(obj, value);
            }
        }
    }
}
