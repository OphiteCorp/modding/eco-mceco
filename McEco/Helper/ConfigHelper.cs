﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace McEco.Lib.Helper
{
    public sealed class ConfigHelper
    {
        public static Dictionary<string, string?> CreateOrLoad(string file, Dictionary<string, string?> defaultConfig)
        {
            Dictionary<string, string?> config = defaultConfig;

            if (!File.Exists(file))
            {
                var json = JsonSerializer.Serialize(config, new JsonSerializerOptions
                {
                    WriteIndented = true
                });
                File.WriteAllText(file, json);
            }
            else
            {
                var json = File.ReadAllText(file);
                return JsonSerializer.Deserialize<Dictionary<string, string?>>(json);
            }
            return defaultConfig;
        }
    }
}
