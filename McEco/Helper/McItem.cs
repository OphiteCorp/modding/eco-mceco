﻿using Eco.Gameplay.Components;
using Eco.Gameplay.Housing;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McEco.Lib.Helper
{
    public static class McItem
    {
        public static IEnumerable<Item> AllItems => Item.AllItems;

        public static Dictionary<Type, WorldObjectItem> AllWorldObjectItems => AllItems.OfType<WorldObjectItem>().ToDictionary(item => item.WorldObjectType, item => item);

        public static IEnumerable<WorldObjectItem> AllHousingItems => AllItems.OfType<WorldObjectItem>().Where(item => item.RequiresComponent<HousingComponent>());

        public static IEnumerable<WorldObjectItem> AllPublicStorageItems => AllItems.OfType<WorldObjectItem>().Where(item => item.RequiresComponent<PublicStorageComponent>());
    }
}
