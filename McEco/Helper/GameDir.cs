﻿namespace McEco.Lib.Helper
{
    public enum GameDir
    {
        Configs,
        Logs,
        Mods,
        Storage
    }
}
