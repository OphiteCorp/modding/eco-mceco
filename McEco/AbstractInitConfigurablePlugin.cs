﻿using Eco.Core.Plugins;
using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;

namespace McEco.Lib
{
    public abstract class AbstractInitConfigurablePlugin : AbstractInitPlugin, IConfigurablePlugin, IEditablePlugin, IGUIPlugin, IDisplayablePlugin, IHasDisplayTabs, IDisplayTab
    {
        public ThreadSafeAction<object, string>? ParamChanged { get; set; }

        public abstract IPluginConfig PluginConfig { get; }

        public abstract object GetEditObject();
        public abstract void OnEditObjectChanged(object o, string param);

        public virtual string GetDisplayText()
        {
            return "Status: " + Status;
        }
    }
}