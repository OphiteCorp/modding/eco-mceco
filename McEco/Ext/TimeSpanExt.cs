﻿using System;

namespace McEco.Lib.Ext
{
    public static class TimeSpanExt
    {
        public static string? ToFormatted(this TimeSpan time)
        {
            return string.Format("{0}.{1}:{2}:{3}", time.Days, time.Hours, time.Minutes, time.Seconds);
        }

        public static string? ToFormattedIncremental(this TimeSpan time)
        {
            if (time.Days > 0)
            {
                return string.Format("{0}d {1}h {2}m {3}s", time.Days, time.Hours, time.Minutes, time.Seconds);
            }
            if (time.Hours > 0)
            {
                return string.Format("{0}h {1}m {2}s", time.Hours, time.Minutes, time.Seconds);
            }
            if (time.Minutes > 0)
            {
                return string.Format("{0}m {1}s", time.Minutes, time.Seconds);
            }
            return string.Format("{0}s", time.Seconds);
        }
    }
}
