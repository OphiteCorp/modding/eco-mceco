﻿using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Gameplay.Utils;
using System;
using System.Linq;
using System.Reflection;

namespace McEco.Lib.Ext
{
    public static class ItemExt
    {
        public static Item ClosestMatchingEntity(this string itemName, User user)
        {
            return CommandsUtil.ClosestMatchingEntity(user, itemName, Item.AllItems, item => item.GetType().Name, item => item.DisplayName);
        }

        public static Item[] GetSkillScrollItems()
        {
            return Item.AllItems.OfType<SkillScroll>().OrderBy(i => i.DisplayName).ToArray();
        }

        public static void SetCustomStackSize(this Type itemType, int stackSize)
        {
            var property = typeof(MaxStackSizeAttribute)?.GetProperty("MaxStackSize", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (property != null && ItemAttribute.TryGet<MaxStackSizeAttribute>(itemType, out MaxStackSizeAttribute itemAttribute))
            {
                property?.SetValue(itemAttribute, stackSize);
            }
        }
    }
}
