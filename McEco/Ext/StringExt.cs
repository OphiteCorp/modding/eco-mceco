﻿using Eco.Gameplay.Items;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using McEco.Lib.Helper;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace McEco.Lib.Ext
{
    public static class StringExt
    {
        public static void ToConsole(this string message, string name, string? group = null)
        {
            if (group != null)
            {
                Log.WriteLine(new LocString($"[{name}] [{group}] ").ConcatStr(message));
            }
            else
            {
                Log.WriteLine(new LocString($"[{name}] ").ConcatStr(message));
            }
        }

        public static void ToConsole(this string message, string? group = null)
        {
            message.ToConsole(AbstractInitPlugin.DefaultCategory, group);
        }

        public static void ToConsoleDebug(this string message)
        {
            message.ToConsole("Debug");
        }

        public static void WriteToLogFile(this string message, string logFile)
        {
            string? filePath = Tools.GetAbsoluteFilePath(GameDir.Logs, logFile);

            if (filePath != null)
            {
                File.AppendAllLines(filePath, new string[] { message }, Encoding.UTF8);
            }
        }

        public static Recipe CreateRecipe(this string name, string? displayName, IngredientElement[] ingredients, CraftingElement[] craftingElements)
        {
            string defDisplayName = name; // $"<color=#E50000>{name[0]}</color><color=white>{name.Substring(1)}</color>";
            Recipe recipe = new();
            recipe.Init(name, Localizer.DoStr(displayName ?? defDisplayName), new List<IngredientElement>(ingredients), new List<CraftingElement>(craftingElements));
            return recipe;
        }

        public static Recipe CreateRecipe(this string name, IngredientElement[] ingredients, CraftingElement[] craftingElements)
        {
            return CreateRecipe(name, null, ingredients, craftingElements);
        }
    }
}
