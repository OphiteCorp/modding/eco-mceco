﻿using McEco.Lib.Helper;
using System.IO;
using System.Text;
using System.Text.Json;

namespace McEco.Lib.Ext
{
    public static class ConfigExt
    {
        public static string? Export<T>(this T config, string configFile)
        {
            string json = JsonSerializer.Serialize(config, new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true,
                WriteIndented = true
            });

            string? filePath = Tools.GetAbsoluteFilePath(GameDir.Configs, configFile);

            if (filePath == null)
            {
                return null;
            }
            File.AppendAllLines(filePath, new string[] { json }, Encoding.UTF8);
            return json;
        }
    }
}
