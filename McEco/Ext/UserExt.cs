﻿using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.NewTooltip.TooltipLibraryFiles;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using McEco.Lib.Helper;
using System;
using System.Net;

namespace McEco.Lib.Ext
{
    public static class UserExt
    {
        public static IPAddress? IpAddress(this User user)
        {
            try
            {
                return ReflectionUtils.GetPropertyByName<IPEndPoint>(user.Client.Connection, "RemoteEndPoint").Address;
            }
            catch (Exception e)
            {
                e.Message?.ToConsole();
                return null;
            }
        }

        public static string? TotalPlayTimeFormatted(this User user)
        {
            return (user != null) ? TimeSpan.FromSeconds(user.TotalPlayTime).ToFormatted() : null;
        }

        public static int TotalStarsEarned(this User user)
        {
            return (user != null && user.UserXP != null) ? user.UserXP.TotalStarsEarned : 0;
        }

        public static LocString FoodStatus(this User user)
        {
            if (user != null && user.Stomach != null)
            {
                return user.Stomach.FoodStatus();
            }
            return LocString.Empty;
        }

        public static float NextLevelPercentSafe(this User user, int decimals = 0)
        {
            if (user != null && user.UserXP != null)
            {
                return (float)Math.Round((user.UserXP.XP / user.UserXP.NextStarCost) * 100, decimals);
            }
            return 0;
        }

        public static float ReputationSafe(this User user)
        {
            return (user != null) ? (float)Math.Round(user.Reputation, 3) : 0;
        }

        public static string? SlgIdSafe(this User user)
        {
            return user?.SlgId;
        }

        public static string? SteamIdSafe(this User user)
        {
            return user?.SteamId;
        }

        public static float HousingValueSafe(this User user)
        {
            if (user != null && user.ResidencyPropertyValue != null)
            {
                return user.ResidencyPropertyValue.Value;
            }
            return 0;
        }

        public static float StomachSkillRateSafe(this User user)
        {
            if (user != null && user.Stomach != null)
            {
                return user.Stomach.NutrientSkillRate();
            }
            return 0;
        }

        public static float XpSafe(this User user)
        {
            if (user != null && user.UserXP != null)
            {
                return user.UserXP.XP;
            }
            return 0;
        }

        public static int NextStarCostSafe(this User user)
        {
            if (user != null && user.UserXP != null)
            {
                return user.UserXP.NextStarCost;
            }
            return 0;
        }

        public static float SkillRateSafe(this User user)
        {
            if (user != null && user.UserXP != null)
            {
                return user.UserXP.SkillRate;
            }
            return 0;
        }

        public static int StarsAvailableSafe(this User user)
        {
            if (user != null && user.UserXP != null)
            {
                return user.UserXP.StarsAvailable;
            }
            return 0;
        }

        public static int ProfessionCountSafe(this User user)
        {
            if (user != null && user.Skillset != null)
            {
                return user.Skillset.ProfessionCount;
            }
            return 0;
        }

        public static int SpecialtyCountSafe(this User user)
        {
            if (user != null && user.Skillset != null)
            {
                return user.Skillset.SpecialtyCount;
            }
            return 0;
        }

        public static bool IsActiveSafe(this User user)
        {
            if (user != null && user.Player != null)
            {
                return user.Player.Active;
            }
            return false;
        }

        public static void SendInfoBox(this User user, string title, string message)
        {
            Tools.ShowInfoBox(user, title, message);
        }
    }
}
