﻿using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using McEco.Lib.Ext;

namespace McEco.Lib
{
    public abstract class AbstractInitPlugin : IModKitPlugin, IInitializablePlugin
    {
        public const string DefaultCategory = "McEco";

        public string Category { get; protected set; } = DefaultCategory;

        public string ModuleName { get; protected set; } = "@Undefined";


        private string status = "Waiting";

        public string Status
        {
            get
            {
                return status;
            }
            private set
            {
                $"Status: {value}".ToConsole(ModuleName);
                status = value;
            }
        }

        public string GetCategory()
        {
            return Category;
        }

        public string GetStatus()
        {
            return Status;
        }

        public override string ToString()
        {
            return ModuleName;
        }

        public void Initialize(TimedTask timer)
        {
            Status = "Initializing";
            StartUp(timer);
            Status = "Ready";
        }

        protected abstract void StartUp(TimedTask timer);
    }
}