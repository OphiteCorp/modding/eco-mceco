﻿using System.Text;

namespace McEco.Lib.Worker
{
    /// <summary>
    /// Zakladni vrstva pro task.
    /// </summary>
    public abstract class AbstractWorker : IWorker
    {
        private string? _unlockCode = null;

        /// <summary>
        /// Cas, kdy bude dalsi tick.
        /// </summary>
        protected TimeSpan NextTick { get; set; } = TimeSpan.Zero;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="unlockCode">Odemykaci kod</param>
        public AbstractWorker(string? unlockCode = null)
        {
            _unlockCode = unlockCode;
        }

        /// <summary>
        /// Interval tasku.
        /// </summary>
        /// <returns>Interval</returns>
        protected abstract int GetInterval();

        /// <summary>
        /// Hlavni logika tasku.
        /// </summary>
        /// <param name="workerData">Informace o tasku.</param>
        /// <returns>True, pokud je vse OK</returns>
        protected abstract bool WorkerProc(WorkerData workerData);

        /// <summary>
        /// Pripravi data pred spustenim smycky.
        /// </summary>
        /// <param name="workerData">Informace o tasku.</param>
        protected virtual object? WorkerProcInit(WorkerData workerData)
        {
            return null;
        }

        /// <summary>
        /// Kontrola odemykaciho kodu.
        /// </summary>
        /// <param name="unlockCode">Kod odemceni</param>
        /// <returns>True, pokud je odemceno</returns>
        protected bool IsUnlockCodeValid(string? unlockCode)
        {
            if (_unlockCode == null)
            {
                return true;
            }
            if (_unlockCode != null && unlockCode == null)
            {
                return false;
            }
            return _unlockCode.Equals(unlockCode);
        }

        /// <summary>
        /// Nazev tasku.
        /// </summary>
        /// <returns></returns>
        public virtual string GetName()
        {
            return string.Empty;
        }

        /// <summary>
        /// Update stavu.
        /// </summary>
        /// <param name="status">Stav</param>
        public virtual void UpdateStatus(StringBuilder status)
        {
            // nic
        }

        /// <summary>
        /// Hlavni smycka.
        /// </summary>
        /// <param name="nextTick">Kdy bude dalsi zavolani tasku</param>
        public virtual void Tick(long nextTick)
        {
            NextTick = TimeSpan.FromSeconds(nextTick);
        }

        /// <summary>
        /// Informace o tasku.
        /// </summary>
        /// <returns>Task</returns>
        public WorkerData GetWorkerData()
        {
            return new WorkerData
            {
                Name = GetName(),
                Interval = GetInterval(),
                Worker = WorkerProcInternal,
                WorkerInit = WorkerProcInit
            };
        }

        private bool WorkerProcInternal(WorkerData workerData)
        {
            // update intervalu, protoze ho lze zmenit v konfiguraci za behu
            workerData.Interval = GetInterval();

            return WorkerProc(workerData);
        }
    }
}
