﻿using Eco.Gameplay.Utils;
using Eco.Shared.Utils;
using McEco.Lib.Ext;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace McEco.Lib.Worker
{
    /// <summary>
    /// Pracovni smycka, ktera pomoci jineho vlakna spousti vstupni tasky. Kazdy vstupni task ma vlastni interval.
    /// </summary>
    public abstract class AbstractWorkerLoop
    {
        public const int TickTimesHistorySize = 10;

        public delegate bool WorkerAction(WorkerData workerData);
        public delegate object? WorkerInitAction(WorkerData workerData);

        private readonly List<IWorker> workersList = new();
        private readonly List<WorkerDataWrapper> workerWrappers = new();

        private Thread? thread = null;
        private bool active = false;
        private readonly ConcurrentQueue<long> tickTimesHistory = new(TickTimesHistorySize);

        /// <summary>
        /// Zapne debug rezim.
        /// </summary>
        public bool IsDebug { get; set; } = false;

        /// <summary>
        /// Minimalni tick cas v ms.
        /// </summary>
        public virtual int MinimumTickTime { get; set; } = 100;

        /// <summary>
        /// Cilovy tick cas v ms.
        /// </summary>
        public virtual int TargetTickTime { get; set; } = 1000;

        /// <summary>
        /// Cilovy tick cas bude pevny.
        /// </summary>
        public virtual bool IsFixedTargetTickTime { get; set; } = false;

        /// <summary>
        /// Cas posledniho ticku.
        /// </summary>
        public long LastTickTime { get; private set; } = 0;

        /// <summary>
        /// Poslednich N casu ticku.
        /// </summary>
        public long[] TickTimesHistory => tickTimesHistory.Get();

        /// <summary>
        /// Prumerny cas ticku.
        /// </summary>
        public float AverageTickTime {
            get
            {
                lock (tickTimesHistory)
                {
                    return (float)Math.Round(TickTimesHistory.Average(), 3);
                }
            }
        }

        /// <summary>
        /// Seznam tasku.
        /// </summary>
        public List<IWorker> Workers => workersList;

        /// <summary>
        /// Prida novy task.
        /// </summary>
        /// <param name="workerData">Task</param>
        public void AddWorker(IWorker worker)
        {
            lock (workersList)
            {
                workersList.Add(worker);

                WorkerData workerData = worker.GetWorkerData();
                workerWrappers.Add(new WorkerDataWrapper
                {
                    Worker = worker,
                    Data = workerData,
                    LastTick = DateTime.Now.AddSeconds(-workerData.Interval)
                });
            }
        }

        /// <summary>
        /// Odebere vsechny tasky.
        /// </summary>
        public void ResetWorkers()
        {
            workerWrappers.Clear();
            workersList.Clear();
        }

        /// <summary>
        /// Stav hlavniho vlakna. V pripade nastaveni umoznuje hlavni vlakno spustit i zastavit.
        /// </summary>
        public bool IsActive
        {
            get { return active; }
            set
            {
                if (active == value)
                {
                    return;
                }
                active = value;

                if (active)
                {
                    Stop();
                    Start();
                }
                else
                {
                    Stop();
                }
            }
        }

        /// <summary>
        /// Nazev hlavni smycky.
        /// </summary>
        /// <returns>Nazev</returns>
        protected abstract string GetName();

        /// <summary>
        /// Update dat pred jejich zpracovanim.
        /// </summary>
        protected virtual void TickUpdate()
        {
            // nic
        }

        /// <summary>
        /// Spusti hlavni vlakno, ktere obsluhuje vsechny tasky.
        /// </summary>
        private void Start()
        {
            if (thread == null)
            {
                thread = new(LoopProc)
                {
                    Priority = ThreadPriority.Normal,
                    IsBackground = true,
                    Name = $"{GetName()}_Thread"
                };
                thread.Start();
                $"Started".ToConsole(GetName());
            }
        }

        /// <summary>
        /// Zastavi hlavni vlakno.
        /// </summary>
        private void Stop()
        {
            if (thread != null)
            {
                try
                {
                    thread.Interrupt();
                }
                catch (Exception e)
                {
                    Log.WriteException(e);
                }
                thread = null;
                $"Stopped".ToConsole(GetName());
            }
        }

        /// <summary>
        /// Logika hlavniho vlakna.
        /// </summary>
        private void LoopProc()
        {
            try
            {
                foreach (WorkerDataWrapper wrapper in workerWrappers)
                {
                    WorkerData? data = wrapper.Data;
                    if (data != null)
                    {
                        wrapper.LastTick = DateTime.Now.AddSeconds(-data.Interval);
                        try
                        {
                            data.InitObject = data.WorkerInit?.Invoke(data);
                        }
                        catch (Exception e)
                        {
                            Log.WriteException(e);
                            // pri initu nastala chyba a proto pro jistotu vypneme worker
                            data.Worker = null;
                        }
                    }
                }
                while (IsActive)
                {
                    // moznost aktualizovat data ve smycce
                    TickUpdate();

                    // jak dlouho trva zpracovani vsech tasku
                    Stopwatch allWorkersTime = Stopwatch.StartNew();
                    try
                    {
                        foreach (WorkerDataWrapper wrapper in workerWrappers)
                        {
                            WorkerData? data = wrapper.Data;

                            // nelze spustit task bez logiky
                            if (data == null || data.Worker == null)
                            {
                                continue;
                            }
                            try
                            {
                                // 1 sekunda pro realnejsi chovani v zobrazeni
                                DateTime interval = DateTime.Now.AddSeconds(-data.Interval);
                                long nextTick = (long) Math.Max(0, Math.Abs(wrapper.LastTick.TotalSeconds() - interval.TotalSeconds() + 1));
                                try
                                {
                                    wrapper.Worker.Tick(nextTick);
                                }
                                catch (ThreadInterruptedException e)
                                {
                                    IsActive = false;
                                    return;
                                }
                                catch (Exception e)
                                {
                                    Log.WriteException(e);
                                }
                                // kazdy task ma vlastni interval, takze i kdyz hlavni vlakno bezi rychleji, tak se task vykona ve vlastnim pravidelnem intervalu
                                if (wrapper.LastTick < interval)
                                {
                                    Stopwatch workerTime = Stopwatch.StartNew();
                                    bool success = data.Worker(data);
                                    workerTime.Stop();

                                    if (success)
                                    {
                                        if (IsDebug)
                                        {
                                            float elapsedTime = (float)Math.Round(allWorkersTime.ElapsedMilliseconds / 1000f, 8);
                                            $"{data} > OK! Time: {elapsedTime}".ToConsole(GetName());
                                        }
                                    }
                                    wrapper.LastTick = DateTime.Now;
                                }
                            }
                            catch (ThreadInterruptedException e)
                            {
                                IsActive = false;
                                return;
                            }
                            catch (Exception e)
                            {
                                Log.WriteException(e);

                                if (IsDebug)
                                {
                                    $"{data} > Failed!".ToConsole(GetName());
                                    $"{data} > {e.Message}".ToConsole(GetName());
                                }
                            }
                        }
                    }
                    catch (ThreadInterruptedException e)
                    {
                        IsActive = false;
                        return;
                    }
                    catch (Exception e)
                    {
                        // obecna chyba, ktera by nemela nastat, jinak ukonci cely hlavni vlakno
                        Log.WriteException(e);
                    }
                    allWorkersTime.Stop();

                    // cas zpracovani vsech tasku a pridani do historie
                    LastTickTime = allWorkersTime.ElapsedMilliseconds;
                    tickTimesHistory.Add(LastTickTime);

                    if (IsFixedTargetTickTime)
                    {
                        // nebere na nic ohled
                        Thread.Sleep(TargetTickTime);
                    }
                    else
                    {
                        // ceka pozadovany cas, aby se zpracovani vykonavalo bez extra prodlevy
                        Thread.Sleep((int)Math.Max(MinimumTickTime, TargetTickTime - LastTickTime));
                    }
                }
            }
            catch (ThreadInterruptedException e)
            {
                IsActive = false;
                return;
            }
            catch (Exception e)
            {
                Log.WriteException(e);
            }
            IsActive = false;
        }

        /// <summary>
        /// Vnitrni informace o tasku.
        /// </summary>
        private sealed class WorkerDataWrapper
        {
            /// <summary>
            /// Worker.
            /// </summary>
            public IWorker Worker { get; set; }

            /// <summary>
            /// Informace o tasku, ktere byly nadefinovane uzivatelem.
            /// </summary>
            public WorkerData? Data { get; set; }

            /// <summary>
            /// Datum posledniho ticku. Slouzi pro spravnou funkci intervalu tasku.
            /// </summary>
            public DateTime LastTick { get; set; }
        }

        /// <summary>
        /// Fronta, ktera ma pevny pocet prvku a pokud se prekroci, tak se zacnou nejstarsi odmazavat.
        /// </summary>
        /// <typeparam name="T">Typ hodnoty</typeparam>
        private sealed class ConcurrentQueue<T>
        {
            private readonly int size;
            private readonly T[] queue;
            private int position = 0;

            /// <summary>
            /// Velikost fronty.
            /// </summary>
            /// <param name="size">Velikost</param>
            public ConcurrentQueue(int size)
            {
                this.size = size;
                queue = new T[size];
            }

            /// <summary>
            /// Pridani prvku do fronty.
            /// </summary>
            /// <param name="value">Hodnota</param>
            public void Add(T value)
            {
                lock (queue)
                {
                    queue[position++] = value;

                    if (position == size)
                    {
                        position = 0;
                    }
                }
            }

            /// <summary>
            /// Ziska celou frontu.
            /// </summary>
            /// <returns>Pole hodnot</returns>
            public T[] Get()
            {
                return queue;
            }
        }
    }
}
