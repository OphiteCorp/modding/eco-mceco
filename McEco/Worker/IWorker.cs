﻿using System.Text;

namespace McEco.Lib.Worker
{
    /// <summary>
    /// Task.
    /// </summary>
    public interface IWorker
    {
        /// <summary>
        /// Nazev tasku.
        /// </summary>
        /// <returns></returns>
        string GetName();

        /// <summary>
        /// Informace o tasku.
        /// </summary>
        /// <returns>Task</returns>
        WorkerData GetWorkerData();

        /// <summary>
        /// Status jobu.
        /// </summary>
        /// <param name="status">Stav</param>
        void UpdateStatus(StringBuilder status);

        /// <summary>
        /// Hlavni smycka.
        /// </summary>
        /// <param name="nextTick">Kdy bude dalsi zavolani tasku</param>
        void Tick(long nextTick);
    }
}
