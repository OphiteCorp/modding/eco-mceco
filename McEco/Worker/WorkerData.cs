﻿using static McEco.Lib.Worker.AbstractWorkerLoop;

namespace McEco.Lib.Worker
{
    /// <summary>
    /// Informace o tasku.
    /// </summary>
    public class WorkerData
    {
        /// <summary>
        /// Název tasku (pro konzoli).
        /// </summary>
        public string Name { get; set; } = "NULL";

        /// <summary>
        /// Interval v sekundach po jakych se ma task zpracovat. Pokud bude 0, tak se ignoruje.
        /// </summary>
        public long Interval { get; set; } = 0;

        /// <summary>
        /// Pripravny objekt.
        /// </summary>
        public object? InitObject { get; set; } = null;

        /// <summary>
        /// Logika tasku.
        /// </summary>
        public WorkerAction? Worker { get; set; }

        /// <summary>
        /// Pripravna logika tasku.
        /// </summary>
        public WorkerInitAction? WorkerInit { get; set; }

        public override string ToString()
        {
            return (Name.Trim().Length == 0) ? "NULL" : Name;
        }
    }
}
