﻿using Eco.Shared.Localization;
using System.Collections.Generic;
using System.ComponentModel;

namespace McEco.Worker.Configs
{
    [DisplayName("Configuration")]
    public class Config
    {
        [DisplayName("Experimental unlock code")]
        [Description("A code that is used to unlock some functions (mostly experimental or paid).")]
        public string UnlockCode { get; set; } = string.Empty;

        // Worker
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Worker")]
        [DisplayName("Active")]
        [Description("Enables or disables the main worker that handles everything.")]
        public bool Worker_Active { get; set; } = true;

        [LocCategory("Worker")]
        [DisplayName("Debug")]
        [Description("Enables debug mode.")]
        public bool Worker_Debug { get; set; } = false;

        [LocCategory("Worker")]
        [DisplayName("Minimum tick time")]
        [Description("Time in milliseconds.")]
        public int Worker_MinimumTickTime { get; set; } = 100;

        [LocCategory("Worker")]
        [DisplayName("Target tick time")]
        [Description("Time in milliseconds.")]
        public int Worker_TargetTickTime { get; set; } = 1000;

        [LocCategory("Worker")]
        [DisplayName("Fixed target tick time")]
        [Description("The target time will be fixed and there will be no concern about minimum or elapsed time.")]
        public bool Worker_FixedTargetTickTime { get; set; } = false;

        // Auto Feeding
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Auto Feeding")]
        [DisplayName("Enabled")]
        [Description("Automatically adds specific food to the stomach (feeds the player).")]
        public bool AutoFeeding_IsEnabled { get; set; } = false;

        [LocCategory("Auto Feeding")]
        [DisplayName("Only online players")]
        [Description("Only applied when the player is online.")]
        public bool AutoFeeding_IsOnlyOnline { get; set; } = false;

        [LocCategory("Auto Feeding")]
        [DisplayName("Debug")]
        [Description("Enables debug mode.")]
        public bool AutoFeeding_IsDebug { get; set; } = false;

        [LocCategory("Auto Feeding")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float AutoFeeding_Interval { get; set; } = 10f;

        [LocCategory("Auto Feeding")]
        [DisplayName("Affected players")]
        [Description("List of affected players. If there is only one with the value '*', then it applies to all players.")]
        public List<string> AutoFeeding_AffectedPlayers { get; set; } = new();

        // Auto Repair
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Auto Repair")]
        [DisplayName("Enabled")]
        [Description("Automatically repairs tools in the player's inventory.")]
        public bool AutoRepair_IsEnabled { get; set; } = false;

        [LocCategory("Auto Repair")]
        [DisplayName("Only online players")]
        [Description("Only applied when the player is online.")]
        public bool AutoRepair_IsOnlyOnline { get; set; } = false;

        [LocCategory("Auto Repair")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float AutoRepair_Interval { get; set; } = 10f;

        [LocCategory("Auto Repair")]
        [DisplayName("Repair value")]
        [Description("Item repair value. Examples: 100 = 100%, 1 = 1%")]
        public float AutoRepair_Value { get; set; } = 1f;

        [LocCategory("Auto Repair")]
        [DisplayName("Affected players")]
        [Description("List of affected players. If there is only one with the value '*', then it applies to all players.")]
        public List<string> AutoRepair_AffectedPlayers { get; set; } = new();

        // Auto Turn Off CO² Machines
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Auto Turn Off CO² Machines")]
        [DisplayName("Enabled")]
        [Description("Automatic monitoring of exceeding PPM. If it occurs, it will shut down all machines that produce CO².")]
        public bool AutoTurnOffCo2Machines_IsEnabled { get; set; } = false;

        [LocCategory("Auto Turn Off CO² Machines")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float AutoTurnOffCo2Machines_Interval { get; set; } = 5f;

        [LocCategory("Auto Turn Off CO² Machines")]
        [DisplayName("Limit value")]
        [Description("The PPM value from which all CO² generators should be switched off.")]
        public int AutoTurnOffCo2Machines_LimitValue { get; set; } = 420;

        // CO² Booster
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("CO² Booster")]
        [DisplayName("Enabled")]
        [Description("Automatically increases CO² in the air.")]
        public bool Co2Booster_IsEnabled { get; set; } = false;

        [LocCategory("CO² Booster")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float Co2Booster_Interval { get; set; } = 60f;

        [LocCategory("CO² Booster")]
        [DisplayName("Value")]
        [Description("How much should PPM increase.")]
        public float Co2Booster_Value { get; set; } = 0.05f;

        [LocCategory("CO² Booster")]
        [DisplayName("Maximum value")]
        [Description("To what maximum value should PPM increase.")]
        public float Co2Booster_MaxValue { get; set; } = 1000f;

        // Sea Booster
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Sea Booster")]
        [DisplayName("Enabled")]
        [Description("Automatic sea level rise.")]
        public bool SeaBooster_IsEnabled { get; set; } = false;

        [LocCategory("Sea Booster")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float SeaBooster_Interval { get; set; } = 60f;

        [LocCategory("Sea Booster")]
        [DisplayName("Value")]
        [Description("How much will the sea level rise.")]
        public float SeaBooster_Value { get; set; } = 0.01f;

        // XP Booster
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("XP Booster")]
        [DisplayName("Enabled")]
        [Description("Automatically adds XP to the players.")]
        public bool XpBooster_IsEnabled { get; set; } = false;

        [LocCategory("XP Booster")]
        [DisplayName("Only online players")]
        [Description("Only applied when the player is online.")]
        public bool XpBooster_IsOnlyOnline { get; set; } = false;

        [LocCategory("XP Booster")]
        [DisplayName("Debug")]
        [Description("Enables debug mode.")]
        public bool XpBooster_IsDebug { get; set; } = false;

        [LocCategory("XP Booster")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float XpBooster_Interval { get; set; } = 60f;

        [LocCategory("XP Booster")]
        [DisplayName("Modifier")]
        [Description("If it is 1, then the next level will be reached in 24 hours only using boost. Examples: 0.5 = 36h, 0.25 = 48h, 1 = 24, 2 = 12h, 3 = 6h")]
        public float XpBooster_Modifier { get; set; } = 0.25f;

        [LocCategory("XP Booster")]
        [DisplayName("Affected players")]
        [Description("List of affected players. If there is only one with the value '*', then it applies to all players.")]
        public List<string> XpBooster_AffectedPlayers { get; set; } = new();

        // World Health Monitor
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("World Health Monitor")]
        [DisplayName("Enabled")]
        [Description("Monitoring PPM status and sea level.")]
        public bool WorldHealthMonitor_IsEnabled { get; set; } = false;

        [LocCategory("World Health Monitor")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float WorldHealthMonitor_Interval { get; set; } = 5f;

        [LocCategory("World Health Monitor")]
        [DisplayName("Sea tolerance")]
        [Description("How much does the sea level have to rise for a notification to occur.")]
        public float WorldHealthMonitor_SeaLevelTolerance { get; set; } = 0.1f;

        [LocCategory("World Health Monitor")]
        [DisplayName("CO² limit")]
        [Description("The PPM value that already determines the exceedance.")]
        public int WorldHealthMonitor_Co2Limit { get; set; } = 420;

        [LocCategory("World Health Monitor")]
        [DisplayName("PPM warn period")]
        [Description("How often should notifications be sent in seconds when PPM is exceeded.")]
        public int WorldHealthMonitor_Co2WarnPeriod { get; set; } = 60 * 30; // 30 min

        // First Day Forever
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("First Day Forever")]
        [DisplayName("Enabled (requires unlocking)")]
        [Description("Day 1 never ends. Attention! Players will not be able to gain experience due to the time shift! For testing purposes only. May not be used publicly in the server list!")]
        public bool FirstDayForever_IsEnabled { get; set; } = false;

        [LocCategory("First Day Forever")]
        [DisplayName("Debug")]
        [Description("Enables debug mode.")]
        public bool FirstDayForever_IsDebug { get; set; } = false;

        // Clean Rubbles
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Clean Rubbles")]
        [DisplayName("Enabled")]
        [Description("Removes all rubbles from the world.")]
        public bool CleanRubbles_IsEnabled { get; set; } = false;

        [LocCategory("Clean Rubbles")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float CleanRubbles_Interval { get; set; } = 60f * 60f * 12f; // 12h

        // Clean Tree Debris
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Clean Tree Debris")]
        [DisplayName("Enabled")]
        [Description("Removes all tree debris from the world.")]
        public bool CleanTreeDebris_IsEnabled { get; set; } = false;

        [LocCategory("Clean Tree Debris")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float CleanTreeDebris_Interval { get; set; } = 60f * 60f * 12f; // 12h

        // Clean Fallen Trees
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Clean Fallen Trees")]
        [DisplayName("Enabled")]
        [Description("Removes all fallen trees and stumps from the world.")]
        public bool CleanFallenTrees_IsEnabled { get; set; } = false;

        [LocCategory("Clean Fallen Trees")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float CleanFallenTrees_Interval { get; set; } = 60f * 60f * 12f; // 12h

        // Clean Blocks
        // ---------------------------------------------------------------------------------------------------------------------

        [LocCategory("Clean Blocks")]
        [DisplayName("Enabled")]
        [Description("Removes all defined blocks from the world.")]
        public bool CleanBlocks_IsEnabled { get; set; } = false;

        [LocCategory("Clean Blocks")]
        [DisplayName("Interval")]
        [Description("Interval in seconds.")]
        public float CleanBlocks_Interval { get; set; } = 60f * 60f * 12f; // 12h

        [LocCategory("Clean Blocks")]
        [DisplayName("Blocks")]
        [Description("Names of blocks to be deleted from the world.")]
        public List<string> CleanBlocks_Blocks { get; set; } = new List<string>(new string[] {
            "GarbageBlock",
            "GarbageBagStacked1Decay1Block",
            "GarbageBagStacked1Decay2Block",
            "GarbageBagStacked1Decay3Block",
            "GarbageBagStacked1Decay4Block",
            "GarbageBagStacked2Decay1Block",
            "GarbageBagStacked2Decay2Block",
            "GarbageBagStacked2Decay3Block",
            "GarbageBagStacked2Decay4Block",
            "GarbageBagStacked3Decay1Block",
            "GarbageBagStacked3Decay2Block",
            "GarbageBagStacked3Decay3Block",
            "GarbageBagStacked3Decay4Block",
            "GarbageBagStacked4Decay1Block",
            "GarbageBagStacked4Decay2Block",
            "GarbageBagStacked4Decay3Block",
            "GarbageBagStacked4Decay4Block"
        });
    }
}
