﻿using Eco.Simulation.Time;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System.Text;
using System;
using McEco.Lib.Ext;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky resetuje cas sveta.
    /// Pozor! Pokud se tak stane, tak se výpočet XP rozbije a prakticky přestane fungovat. Aby opět začal, tak je nutné počkat na posunutý čas nebo ho posunout ručně.
    /// Toto zapínat jen v kombinaci s XP Boostem, kdy všichni budou mít konstantní XP rate.
    /// </summary>
    internal sealed class FirstDayForeverWorker : AbstractWorker
    {
        private const int FixedInterval = 1000 * 60 * 2; // 2 min

        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _timeToReset = 0;

        public FirstDayForeverWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "First Day Forever";
        }

        protected override int GetInterval()
        {
            return FixedInterval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.FirstDayForever_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Time to reset: {(enabled ? TimeSpan.FromSeconds(_timeToReset).ToFormatted() : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _timeToReset = 0;

            if (!Config.FirstDayForever_IsEnabled)
            {
                return false;
            }
            if (!IsUnlockCodeValid(Config.UnlockCode))
            {
                "Cannot be done because the service requires unlocking.".ToConsole(GetName());
                return false;
            }
            double seconds = WorldTime.Seconds;

            // pokud bude stari sveta vetsi nez 23h a 50min .. (10min pred zacatnem noveho dne)
            if (seconds > (86400 - 600))
            {
                WorldTime.ResetOffset();
                WorldTime.Reset();

                if (Config.FirstDayForever_IsDebug)
                {
                    "The world time has been reset to 0.".ToConsole(GetName());
                }
            }
            _timeToReset = Math.Max(0, (int)seconds - (86400 - 600));
            return true;
        }
    }
}
