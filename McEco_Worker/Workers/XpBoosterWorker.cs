﻿using Eco.Gameplay.Players;
using Eco.Shared.Utils;
using Eco.Simulation.Time;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky přidává XP hracum.
    /// </summary>
    internal sealed class XpBoosterWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _updatedPlayers = 0;
        private double _totalXp = 0;

        public XpBoosterWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "XP Booster";
        }

        protected override int GetInterval()
        {
            return (int)Config.XpBooster_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.XpBooster_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Updated players: {(enabled ? _updatedPlayers : "-")}");
            status.AppendLine($"Total XP added to players: {(enabled ? Math.Round(_totalXp, 6) : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _updatedPlayers = 0;
            _totalXp = 0;

            if (!Config.XpBooster_IsEnabled || Config.XpBooster_AffectedPlayers.Count == 0)
            {
                return false;
            }
            IEnumerable<User> users;
            // pokud misto seznam hracu bude pouze jeden jako "*", tak se pouziji vsichni hraci na serveru
            if (Config.XpBooster_AffectedPlayers.Count == 1 && Config.XpBooster_AffectedPlayers.First().Equals("*"))
            {
                users = UserManager.Users;
            }
            else
            {
                users = Config.XpBooster_AffectedPlayers.Select(p => UserManager.FindUserByName(p));
            }
            // projde hrace, kterym se ma pridat XP
            users.Where(p => (Config.XpBooster_IsOnlyOnline && p.IsOnline) || !Config.XpBooster_IsOnlyOnline).OrderBy(p => p.Name).ForEach(p => {
                int levelMaxXp = p.NextStarCostSafe();
                int currentStars = p.StarsAvailableSafe();

                // lze shodit hru, maximalni level je kolem 500
                if (levelMaxXp > 0 && currentStars < 100)
                {
                    float xp = (levelMaxXp / 24f) / 60f;
                    xp *= Config.XpBooster_Modifier;
                    p.UserXP.AddExperience(xp);

                    _updatedPlayers++;
                    _totalXp += xp;

                    if (Config.XpBooster_IsDebug)
                    {
                        $"Player {p.Name} ({p.TotalStarsEarned()} level) got {Math.Round(xp, 6)} XP ({Math.Round(p.XpSafe(), 8)} / {levelMaxXp}) | NextTick: {Math.Round(p.NextTick)}, WorldTime: {Math.Round(WorldTime.Seconds)}".ToConsole(GetName());
                    }
                }
            });
            return true;
        }
    }
}
