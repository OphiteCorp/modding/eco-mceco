﻿using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Shared.Utils;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky opravuje nástroje v inventari hrace.
    /// </summary>
    internal sealed class AutoRepairWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _completelyRepaired = 0;
        private int _repaired = 0;

        public AutoRepairWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Auto Repair";
        }

        protected override int GetInterval()
        {
            return (int)Config.AutoRepair_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.AutoRepair_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Repaired: {(enabled ? _repaired : "-")}");
            status.AppendLine($"Completely repaired: {(enabled ? _completelyRepaired : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _repaired = 0;
            _completelyRepaired = 0;

            if (!Config.AutoRepair_IsEnabled || Config.AutoRepair_AffectedPlayers.Count == 0)
            {
                return false;
            }
            IEnumerable<User>? users;
            // pokud misto seznam hracu bude pouze jeden jako "*", tak se pouziji vsichni hraci na serveru
            if (Config.AutoRepair_AffectedPlayers.Count == 1 && Config.AutoRepair_AffectedPlayers.First().Equals("*"))
            {
                users = UserManager.Users;
            }
            else
            {
                users = Config.AutoRepair_AffectedPlayers.Select(p => UserManager.FindUserByName(p));
            }
            // projde hrace, kterym se maji opravovat predmety
            users?.Where(p => p != null && ((Config.AutoRepair_IsOnlyOnline && p.IsOnline) || !Config.AutoRepair_IsOnlyOnline)).OrderBy(p => p.Name).ForEach(p =>
            {
                // veci v batohu
                var inventoryStacks = p.Inventory?.Backpack.NonEmptyStacks ?? null;
                if (inventoryStacks != null)
                {
                    inventoryStacks.ForEach(s => {
                        Repair(s, Config.AutoRepair_Value, out bool repaired, out bool completelyRepaired);
                        _repaired += repaired ? 1 : 0;
                        _completelyRepaired += completelyRepaired ? 1 : 0;
                    });
                }
                // dolni lista s predmety
                var toolbarStacks = p.Inventory?.Toolbar?.NonEmptyStacks ?? null;
                if (toolbarStacks != null)
                {
                    toolbarStacks.ForEach(s => {
                        Repair(s, Config.AutoRepair_Value, out bool repaired, out bool completelyRepaired);
                        _repaired += repaired ? 1 : 0;
                        _completelyRepaired += completelyRepaired ? 1 : 0;
                    });
                }
            });
            return true;
        }

        private static void Repair(ItemStack stack, float repairValue, out bool repaired, out bool completelyRepaired)
        {
            repaired = false;
            completelyRepaired = false;

            if (stack.Item is ToolItem && stack.Item is RepairableItem repairable)
            {
                lock (repairable)
                {
                    if (repairable.DurabilityRate < 1.0f)
                    {
                        float newDurability = Math.Min(DurabilityItem.DurabilityMax, repairable.Durability + repairValue);
                        repairable.Durability = newDurability;
                        
                        if (newDurability < DurabilityItem.DurabilityMax)
                        {
                            repaired = true;
                        }
                        else
                        {
                            completelyRepaired = true;
                        }
                    }
                }
            }
        }
    }
}
