﻿using Eco.Shared.Math;
using Eco.Shared.Utils;
using Eco.World;
using Eco.World.Blocks;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky cisti vetve stromu.
    /// </summary>
    internal sealed class CleanTreeDebrisWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _deletedDebris = 0;

        public CleanTreeDebrisWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Clean Tree Debris";
        }

        protected override int GetInterval()
        {
            return (int)Config.CleanTreeDebris_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.CleanTreeDebris_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Deleted tree debris: {(enabled ? _deletedDebris : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _deletedDebris = 0;

            if (!Config.CleanTreeDebris_IsEnabled)
            {
                return false;
            }
            IEnumerable<Vector3i> targetPositions = World.TopBlockCache
                        .Select((pos, block) => { return World.GetTopGroundPos(pos) + Vector3i.Up; })
                        .Where(pos => World.GetBlock(pos).Is<TreeDebris>());

            int total = 0;
            int deleted = 0;

            if (targetPositions != null)
            {
                total = targetPositions.Count();
                targetPositions.ForEach(pos => {
                    try
                    {
                        World.DeleteBlock(pos);
                        deleted++;
                    }
                    catch (ThreadInterruptedException)
                    {
                        // nic
                    }
                    catch (Exception e)
                    {
                        Log.WriteException(e);
                    }
                });
                _deletedDebris = deleted;
            }
            return true;
        }
    }
}
