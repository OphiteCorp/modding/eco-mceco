﻿using Eco.Gameplay.Blocks;
using Eco.Shared.Math;
using Eco.Shared.Utils;
using Eco.World;
using Eco.World.Blocks;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky cisti svet od ruznych nadefinovanych bloku.
    /// </summary>
    internal sealed class CleanBlocksWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _deletedBlocks = 0;

        public CleanBlocksWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Clean Blocks";
        }

        protected override int GetInterval()
        {
            return (int)Config.CleanBlocks_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.CleanBlocks_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Deleted blocks: {(enabled ? _deletedBlocks : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _deletedBlocks = 0;

            if (!Config.CleanBlocks_IsEnabled)
            {
                return false;
            }
            bool next = true;
            int total = 0;
            int deleted = 0;

            while (next)
            {
                DeleteBLocks(ref total, ref deleted);
                next = (total > 0);

                if (next)
                {
                    Thread.Sleep(1000);
                }
            }
            _deletedBlocks = deleted;
            return true;
        }

        private void DeleteBLocks(ref int totalOut, ref int deletedOut)
        {
            List<BlockData> result = new();
            List<Type> blockTypes = GetBlockTypes();

            World.Chunks?.Where(p => { return p.Blocks != null && p.Blocks.Any(); }).ForEach(chunk => {
                Vector3i chunkPosition = chunk.Position * Eco.Shared.Voxel.Chunk.Size;
                Block[] blocks = chunk.Blocks?.ToArray() ?? null;

                for (int i = 0; blocks != null && i < Eco.Shared.Voxel.Chunk.Count; i++)
                {
                    Block block = blocks[i];
                    if (block != null)
                    {
                        Vector3i blockPosition = chunkPosition + Eco.Shared.Voxel.Chunk.Location(i);
                        bool isContained = BlockContainerManager.Obj.IsBlockContained(blockPosition);

                        blockTypes?.ForEach(blockType => {
                            if (block.GetType().Equals(blockType) && !isContained)
                            {
                                result.Add(new()
                                {
                                    Position = blockPosition,
                                    Block = block,
                                    BlockType = blockType
                                });
                            }
                        });
                    }
                }
            });

            int total = result.Count;
            int deleted = 0;

            result.ForEach(data => {
                try
                {
                    World.DeleteBlock(data.Position);
                    deleted++;
                }
                catch (ThreadInterruptedException)
                {
                    // nic
                }
                catch (Exception e)
                {
                    Log.WriteException(e);
                }
            });
            totalOut += total;
            deletedOut += deleted;
        }

        private List<Type> GetBlockTypes()
        {
            List<Type> types = new();

            Config.CleanBlocks_Blocks.Where(p => p != null && p.Trim().Length > 0).ForEach(p => {
                try
                {
                    Type? type = BlockManager.BlockTypes.FirstOrDefault(t => t.Name.ToLower() == p.ToLower());
                    if (type != null)
                    {
                        types.Add(type);
                    }
                    else
                    {
                        $"Block type: {p} not exists!".ToConsole(GetName());
                    }
                }
                catch (Exception)
                {
                    $"Block type: {p} not exists!".ToConsole(GetName());
                }
            });
            return types;
        }

        private sealed class BlockData
        {
            public Vector3i Position { get; set; }
            public Block? Block { get; set; }
            public Type? BlockType { get; set; }

            public override string ToString()
            {
                return $"{BlockType?.Name} = {Position}";
            }
        }
    }
}
