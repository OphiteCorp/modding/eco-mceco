﻿using Eco.Shared.Utils;
using Eco.Simulation.WorldLayers;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Text;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky zveda hladinu more.
    /// </summary>
    internal sealed class SeaBoosterWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private float _seaLevel = 0;

        public SeaBoosterWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Sea Booster";
        }

        protected override int GetInterval()
        {
            return (int)Config.SeaBooster_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.SeaBooster_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Sea Level: {(enabled ? Math.Round(_seaLevel, 3) : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _seaLevel = 0;

            if (!Config.SeaBooster_IsEnabled)
            {
                return false;
            }
            var climate = Singleton<WorldLayerManager>.Obj.ClimateSim;
            climate.SetSeaLevel(Math.Max(0f, climate.State.SeaLevel + Config.SeaBooster_Value));

            _seaLevel = climate.State.SeaLevel;
            return true;
        }
    }
}
