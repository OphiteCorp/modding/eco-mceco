﻿using Eco.Gameplay.Components;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Systems.Messaging.Notifications;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using Eco.Simulation.WorldLayers;
using McEco.Lib.Ext;
using McEco.Lib.Helper;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Pokud se prekroci bezpecna hranice PPM, tak vyhleda ve svete vsechny stroje, ktere produkuji CO² a vypne je.
    /// </summary>
    internal sealed class AutoTurnOffCo2MachinesWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _machinesOff = 0;

        public AutoTurnOffCo2MachinesWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Auto Turn Off Co² Machines";
        }

        protected override int GetInterval()
        {
            return (int)Config.AutoTurnOffCo2Machines_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.AutoTurnOffCo2Machines_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Machines turned off: {(enabled ? _machinesOff : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _machinesOff = 0;

            if (!Config.AutoTurnOffCo2Machines_IsEnabled)
            {
                return false;
            }
            // aktualni hodnota CO2 a pokud je vyssi. tak vykona akci
            float totalCo2 = (float)Math.Round(WorldLayerManager.Obj.Climate.TotalCO2, 2);
            if (totalCo2 > Config.AutoTurnOffCo2Machines_LimitValue)
            {
                List<WorldObject> objects = new();

                // vyhleda vsechny objekty, ktere produkuji CO2 a zaroven maji moznost se vypnot
                var pollutionsComponents = WorldObjectUtil.AllObjsWithComponent<AirPollutionComponent>();
                pollutionsComponents.Where(p => p.Parent.CreatingItem.RequiresComponent<OnOffComponent>()).ForEach(p =>
                {
                    // vypne stroj
                    var onOff = p.Parent.Components.Where(c => c is OnOffComponent && c is not VehicleComponent).First() as OnOffComponent;
                    if (onOff != null && onOff.On)
                    {
                        objects.Add(p.Parent);
                        onOff.On = false;
                        _machinesOff++;
                    }
                });
                // pokud se nejaky stroj vypnul, tak odesle udalost o stavu
                if (_machinesOff > 0)
                {
                    StringBuilder sb = new();
                    objects.Select(p => $"{p.UILink()} (<color=#DAB287>Creator:</color> {p.Creator.UILink()})\n").ForEach(p => sb.Append(p));

                    NotificationManager.Msg(new LocString($"PPM exceeded the safe limit of <color=yellow>{totalCo2}</color>. <color=orange>{_machinesOff}</color> objects that produced CO² will be turned OFF:\n{sb}"),
                        Tools.GetOnlineUsers());
                }
            }
            return true;
        }
    }
}