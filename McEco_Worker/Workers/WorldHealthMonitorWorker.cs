﻿using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Messaging.Notifications;
using Eco.Shared.Localization;
using Eco.Simulation.WorldLayers;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Linq;
using System.Text;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky kontroluje a odesila notifikace, pokud je prekocena hodnota CO² nebo more.
    /// </summary>
    internal sealed class WorldHealthMonitorWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private float _totalCo2 = 0;
        private float _seaLevel = 0;

        public WorldHealthMonitorWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "World Health Monitor";
        }

        protected override int GetInterval()
        {
            return (int)Config.WorldHealthMonitor_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.WorldHealthMonitor_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"PPM: {(enabled ? Math.Round(_totalCo2, 3) : "-")}");
            status.AppendLine($"Sea Level: {(enabled ? Math.Round(_seaLevel, 3) : "-")}");
        }

        protected override object? WorkerProcInit(WorkerData workerData)
        {
            float totalCo2 = WorldLayerManager.Obj.Climate.TotalCO2;
            long now = DateTime.Now.Ticks;

            return new InitData
            {
                StartSeaLevel = WorldLayerManager.Obj.Climate.SeaLevel,
                TotalCo2 = totalCo2,
                LastSeaTime = now,
                LastCo2WarnPeriod = now,
                Co2OverLimit = totalCo2 > Config.WorldHealthMonitor_Co2Limit,
                Co2WarnCounter = 0
            };
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _totalCo2 = 0;
            _seaLevel = 0;

            if (!Config.WorldHealthMonitor_IsEnabled)
            {
                return false;
            }
            InitData? init = workerData.InitObject as InitData;
            if (init == null)
            {
                return false;
            }

            // aktualizuje hodnotu CO2
            init.TotalCo2 = WorldLayerManager.Obj.Climate.TotalCO2;
            bool renderSea = false;

            // pokud aktualni CO2 prekroci limitni hodnoty
            if (init.TotalCo2 >= Config.WorldHealthMonitor_Co2Limit)
            {
                init.Co2OverLimit = true;
                // pocet odeslanych notofikaci dokud se PPM nevrati pod bezpecnou hranici
                ++init.Co2WarnCounter;

                long now = DateTime.Now.Ticks;
                long diff = now - init.LastCo2WarnPeriod;

                // pokud prekroceni nastane poprve nebo behem uplynule doby nedojde ke snizeni PPM na bezpecnou hranici, tak odesle udalost
                if (init.Co2WarnCounter == 1 || new TimeSpan(diff).TotalSeconds > Config.WorldHealthMonitor_Co2WarnPeriod)
                {
                    if (init.Co2WarnCounter == 1)
                    {
                        NotificationManager.Msg(
                            new LocString($"<color=red>Attention!</color> CO² has exceeded the safe limit and is <color=yellow>{Math.Round(init.TotalCo2, 2)}</color> ppm. The oceans may begin to rise!"),
                            UserManager.Users.Where(x => x.IsOnline));
                    }
                    else
                    {
                        NotificationManager.Msg(
                            new LocString($"<color=red>Attention!</color> CO² which is <color=yellow>{Math.Round(init.TotalCo2, 2)}</color> ppm is still too high and seas will rise!"),
                            UserManager.Users.Where(x => x.IsOnline));
                    }
                    init.LastCo2WarnPeriod = now;
                    renderSea = true;
                }
            }
            // nastane v pripade, ze doslo k prekroceni PPM a behem doby se opět dostalo na bezpecnou hodnotu
            // v tomto pripade odesle udalost, ze PPM je opet bezpecny
            else if (init.Co2OverLimit)
            {
                init.Co2OverLimit = false;
                init.Co2WarnCounter = 0;

                NotificationManager.Msg(
                    new LocString($"<color=#DAB287>Notice:</color> CO² is again at a safe value of <color=green>{Math.Round(init.TotalCo2, 2)}</color> ppm."),
                    UserManager.Users.Where(x => x.IsOnline));
            }

            // aktualni vyska more a rozdil oproti pocatecni
            float newSeaLevel = WorldLayerManager.Obj.Climate.SeaLevel;
            float seaDiff = newSeaLevel - init.StartSeaLevel;

            // pokud rozdil bude vetsi nez tolerance, tak odesle udalost
            if (seaDiff > Config.WorldHealthMonitor_SeaLevelTolerance && renderSea)
            {
                long now = DateTime.Now.Ticks;
                TimeSpan time = new TimeSpan(now - init.LastSeaTime);
                var timeFormat = time.ToFormattedIncremental();

                NotificationManager.Msg(
                new LocString($"<color=red>Attention!</color> Sea level raised by <color=yellow>{Math.Round(seaDiff, 2)}</color> to <color=yellow>{Math.Round(newSeaLevel, 2)}</color> in time <color=#DAB287>{timeFormat}</color>."),
                    UserManager.Users.Where(x => x.IsOnline));

                init.StartSeaLevel = newSeaLevel;
                init.LastSeaTime = now;
            }
            _totalCo2 = init.TotalCo2;
            _seaLevel = newSeaLevel;
            return true;
        }

        private sealed class InitData
        {
            public float StartSeaLevel { get; set; }
            public float TotalCo2 { get; set; }

            public long LastSeaTime { get; set; }
            public long LastCo2WarnPeriod { get; set; }
            public bool Co2OverLimit { get; set; }
            public int Co2WarnCounter { get; set; }
        }
    }
}
