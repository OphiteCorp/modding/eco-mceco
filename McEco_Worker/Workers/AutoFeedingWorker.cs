﻿using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Shared.Localization;
using Eco.Shared.Serialization;
using Eco.Shared.Utils;
using Eco.Simulation.Time;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky syceni zaludku.
    /// </summary>
    internal sealed class AutoFeedingWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _fedPlayers = 0;
        private int _skippedPlayers = 0;

        public AutoFeedingWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Auto Feeding";
        }

        protected override int GetInterval()
        {
            return (int)Config.AutoFeeding_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.AutoFeeding_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Fed: {(enabled ? _fedPlayers : "-")}");
            status.AppendLine($"Skipped: {(enabled ? _skippedPlayers : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _fedPlayers = 0;
            _skippedPlayers = 0;

            if (!Config.AutoFeeding_IsEnabled || Config.AutoFeeding_AffectedPlayers.Count == 0)
            {
                return false;
            }
            IEnumerable<User>? users;
            // pokud misto seznam hracu bude pouze jeden jako "*", tak se pouziji vsichni hraci na serveru
            if (Config.AutoFeeding_AffectedPlayers.Count == 1 && Config.AutoFeeding_AffectedPlayers.First().Equals("*"))
            {
                users = UserManager.Users;
            }
            else
            {
                users = Config.AutoFeeding_AffectedPlayers.Select(p => UserManager.FindUserByName(p));
            }
            // projde hrace, kterym se ma pridat jidlo
            users?.Where(p => p != null && ((Config.AutoFeeding_IsOnlyOnline && p.IsOnline) || !Config.AutoFeeding_IsOnlyOnline)).OrderBy(p => p.Name).ForEach(p => {
                Stomach stomach = p.Stomach;
                FoodItem food = new DefaultFood();

                if (stomach.Calories + food.Calories <= stomach.MaxCalories)
                {
                    float skillRate = stomach.NutrientSkillRate();

                    stomach.Contents.Add(new StomachEntry
                    {
                        Food = food,
                        TimeEaten = WorldTime.Seconds
                    });
                    stomach.ForceSetCalories(Math.Max(0f, stomach.Calories + food.Calories));
                    stomach.TasteBuds.DoEat(p, food);
                    stomach.Cravings.DoEat(p, food);
                    stomach.RecalcAverageNutrients();

                    if (Config.AutoFeeding_IsDebug)
                    {
                        float skillRateDiff = (float)Math.Abs(Math.Round(stomach.NutrientSkillRate() - skillRate, 2));
                        $"{p.Name} nutrition now provides {Math.Round(stomach.NutrientSkillRate(), 2)} skill per day ({skillRateDiff})".ToConsole(GetName());
                    }
                    _fedPlayers++;
                }
                else
                {
                    _skippedPlayers++;
                }
            });
            return true;
        }

        /// <summary>
        /// Vlastni nove jidlo, ktere se bude pridavat v pravidelnych intervalech do zaludku.
        /// </summary>
        [Serialized]
        [LocDisplayName("Food")]
        [Category("Hidden")]
        [Weight(0)]
        private sealed class DefaultFood : FoodItem
        {
            public override Nutrients Nutrition => new() { Carbs = 25, Fat = 25, Protein = 25, Vitamins = 25 };

            public override float Calories => 500;

            protected override float BaseShelfLife => (float)TimeUtil.HoursToSeconds(24);
        }
    }
}
