﻿using Eco.Shared.Utils;
using Eco.Simulation.WorldLayers;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Text;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky navyšuje CO².
    /// </summary>
    internal sealed class Co2BoosterWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private float _totalCo2 = 0;

        public Co2BoosterWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Co² Booster";
        }

        protected override int GetInterval()
        {
            return (int)Config.Co2Booster_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.Co2Booster_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"PPM: {(enabled ? Math.Round(_totalCo2, 3) : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _totalCo2 = 0;

            if (!Config.Co2Booster_IsEnabled)
            {
                return false;
            }
            // zastaveni zvysovani CO2
            float totalCo2 = (float)Math.Round(WorldLayerManager.Obj.Climate.TotalCO2, 2);
            if (totalCo2 > Config.Co2Booster_MaxValue)
            {
                return false;
            }
            var climate = Singleton<WorldLayerManager>.Obj.ClimateSim;
            float newValue = Math.Max(0f, climate.State.TotalCO2 + Config.Co2Booster_Value);
            climate.State.TotalCO2 += newValue;

            _totalCo2 = climate.State.TotalCO2;
            return true;
        }
    }
}
