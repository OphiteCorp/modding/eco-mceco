﻿using Eco.Gameplay.Objects;
using Eco.Shared.Networking;
using Eco.Shared.Utils;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky cisti svet od ulomku kamenu.
    /// </summary>
    internal sealed class CleanRubblesWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _deletedRubbles = 0;

        public CleanRubblesWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Clean Rubbles";
        }

        protected override int GetInterval()
        {
            return (int)Config.CleanRubbles_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.CleanRubbles_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Deleted rubbles: {(enabled ? _deletedRubbles : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _deletedRubbles = 0;

            if (!Config.CleanRubbles_IsEnabled)
            {
                return false;
            }
            IEnumerable<RubbleObject> rubbles = NetObjectManager.Default.GetObjectsOfType<RubbleObject>();

            int total = 0;
            int deleted = 0;

            if (rubbles != null)
            {
                total = rubbles.Count();
                rubbles.ForEach(obj => {
                    try
                    {
                        obj.Destroy();
                        deleted++;
                    }
                    catch (ThreadInterruptedException)
                    {
                        // nic
                    }
                    catch (Exception e)
                    {
                        Log.WriteException(e);
                    }
                });
                _deletedRubbles = deleted;
            }
            return true;
        }
    }
}
