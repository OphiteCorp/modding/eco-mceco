﻿using Eco.Shared.Networking;
using Eco.Shared.Utils;
using McEco.Lib.Ext;
using McEco.Lib.Worker;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace McEco.Worker.Workers
{
    /// <summary>
    /// Automaticky cisti pokacene stromy a jejich parezy.
    /// </summary>
    internal sealed class CleanFallenTreesWorker : AbstractWorker
    {
        public Config Config { get; private set; } = InitPlugin.Obj.Config;

        private int _deletedFallenTrees = 0;
        private int _deletedStumps = 0;

        public CleanFallenTreesWorker(string? unlockCode = null) : base(unlockCode)
        {
        }

        public override string GetName()
        {
            return "Clean Fallen Trees";
        }

        protected override int GetInterval()
        {
            return (int)Config.CleanFallenTrees_Interval;
        }

        public override void UpdateStatus(StringBuilder status)
        {
            base.UpdateStatus(status);

            bool enabled = Config.CleanFallenTrees_IsEnabled;

            status.AppendLine($"Active: {enabled}");
            status.AppendLine($"Next tick: {(enabled ? NextTick.ToFormatted() : "-")}");
            status.AppendLine($"Deleted fallen trees: {(enabled ? _deletedFallenTrees : "-")}");
            status.AppendLine($"Deleted tree stumps: {(enabled ? _deletedStumps : "-")}");
        }

        protected override bool WorkerProc(WorkerData workerData)
        {
            _deletedFallenTrees = 0;
            _deletedStumps = 0;

            if (!Config.CleanFallenTrees_IsEnabled)
            {
                return false;
            }
            DestroyFallenTrees(out int totalFallenTrees, out int deletedFallenTrees);
            DestroyTreeStumps(out int totalStumps, out int deletedStumps);

            _deletedFallenTrees = deletedFallenTrees;
            _deletedStumps = deletedStumps;
            return true;
        }

        private static void DestroyFallenTrees(out int totalOut, out int deletedOut)
        {
            IEnumerable<TreeEntity> fallenTrees = NetObjectManager.Default.GetObjectsOfType<TreeEntity>()
                  .Where(entity => { return !entity.IsStump && entity.Fallen && entity.PlantPack != null; });

            int total = 0;
            int deleted = 0;

            if (fallenTrees != null)
            {
                total = fallenTrees.Count();
                fallenTrees.ForEach(entity => {
                    try
                    {
                        entity.Destroy();
                        deleted++;
                    }
                    catch (ThreadInterruptedException)
                    {
                        // nic
                    }
                    catch (Exception e)
                    {
                        Log.WriteException(e);
                    }
                });
            }
            totalOut = total;
            deletedOut = deleted;
        }

        private static void DestroyTreeStumps(out int totalOut, out int deletedOut)
        {
            IEnumerable<TreeEntity> treeStumps = NetObjectManager.Default.GetObjectsOfType<TreeEntity>()
                .Where(entity => { return entity.IsStump; });

            int total = 0;
            int deleted = 0;

            if (treeStumps != null)
            {
                total = treeStumps.Count();
                treeStumps.ForEach(entity => {
                    try
                    {
                        entity.Destroy();
                        deleted++;
                    }
                    catch (ThreadInterruptedException)
                    {
                        // nic
                    }
                    catch (Exception e)
                    {
                        Log.WriteException(e);
                    }
                });
            }
            totalOut = total;
            deletedOut = deleted;
        }
    }
}
