﻿using Eco.Core;
using Eco.Core.Plugins;
using Eco.Core.Utils;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Shared.Localization;
using Eco.Shared.Math;
using Eco.Shared.Networking;
using Eco.Shared.Utils;
using Eco.Simulation.WorldLayers;
using Eco.World;
using Eco.World.Blocks;
using McEco.Lib;
using McEco.Lib.Ext;
using McEco.Worker.Configs;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Resources;

namespace McEco.Worker
{
    [LocDisplayName(DefaultModuleName)]
    [Priority(PriorityAttribute.Normal)]
    public class InitPlugin : AbstractInitConfigurablePlugin
    {
        public const string DefaultModuleName = "Worker";

        private readonly PluginConfig<Config> config = new(DefaultCategory + DefaultModuleName);

        public static InitPlugin Obj => PluginManager.GetPlugin<InitPlugin>();

        public Config Config => config.Config;

        public override IPluginConfig PluginConfig => config;

        public InitPlugin()
        {
            ModuleName = DefaultModuleName;
        }

        public override object GetEditObject()
        {
            return config.Config;
        }

        public override void OnEditObjectChanged(object o, string param)
        {
            config.SaveAsync().Wait();
        }

        protected override void StartUp(TimedTask timed)
        {
            string? unlockCode = Properties.Data.ResourceManager.GetString("unlock_code");
            InitWorkers.StartUp(unlockCode);
        }

        public override string GetDisplayText()
        {
            return InitWorkers.Status;
        }
    }
}