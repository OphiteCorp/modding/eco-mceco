﻿using McEco.Worker.Workers;
using McEco.Lib.Worker;
using System.Threading;
using McEco.Worker.Configs;
using System.Text;
using System.Diagnostics;
using System;
using McEco.Lib.Ext;
using System.Linq;
using Eco.Simulation.Time;

namespace McEco.Worker
{
    internal sealed class InitWorkers
    {
        private readonly static WorkerLoop workerLoop = new();

        public static string Status => workerLoop.Status;

        public static void StartUp(string? unlockCode)
        {
            workerLoop.AddWorker(new XpBoosterWorker());
            workerLoop.AddWorker(new AutoFeedingWorker());
            workerLoop.AddWorker(new AutoRepairWorker());
            workerLoop.AddWorker(new SeaBoosterWorker());
            workerLoop.AddWorker(new Co2BoosterWorker());
            workerLoop.AddWorker(new AutoTurnOffCo2MachinesWorker());
            workerLoop.AddWorker(new WorldHealthMonitorWorker());
            workerLoop.AddWorker(new CleanRubblesWorker());
            workerLoop.AddWorker(new CleanTreeDebrisWorker());
            workerLoop.AddWorker(new CleanFallenTreesWorker());
            workerLoop.AddWorker(new CleanBlocksWorker());
            workerLoop.AddWorker(new FirstDayForeverWorker(unlockCode));
            workerLoop.IsActive = true;

            _ = new WorkerMonitor();
        }

        private sealed class WorkerLoop : AbstractWorkerLoop
        {
            public override int MinimumTickTime => InitPlugin.Obj.Config.Worker_MinimumTickTime;
            public override int TargetTickTime => InitPlugin.Obj.Config.Worker_TargetTickTime;
            public override bool IsFixedTargetTickTime => InitPlugin.Obj.Config.Worker_FixedTargetTickTime;

            public string Status { get; set; } = string.Empty;

            protected override string GetName()
            {
                return "WorkerLoop";
            }

            protected override void TickUpdate()
            {
                // provede se pri kazdem ticku vlakna pred zpracovanim workeru
            }
        }

        /// <summary>
        /// Sledovani tasku, aby bylo mozne pres konfiguraci manipulovat s hlavnim workerem.
        /// </summary>
        private sealed class WorkerMonitor
        {
            public WorkerMonitor()
            {
                new Thread(Proc)
                {
                    Priority = ThreadPriority.BelowNormal,
                    IsBackground = true
                }
                .Start();
            }

            private static void Proc()
            {
                Stopwatch stopwatch = new();
                long elapsedSeconds = 0;

                while (Thread.CurrentThread.IsAlive)
                {
                    Config config = InitPlugin.Obj.Config;
                    workerLoop.IsActive = config.Worker_Active;
                    workerLoop.IsDebug = config.Worker_Debug;

                    UpdateStatus(stopwatch, elapsedSeconds);
                    elapsedSeconds++;

                    Thread.Sleep(1000);
                }
            }

            private static void UpdateStatus(Stopwatch stopwatch, long elapsedSeconds)
            {
                StringBuilder sb = new();

                AppendTickHistory(sb);
                AppendInfo(sb, elapsedSeconds);

                sb.AppendLine();

                foreach (IWorker worker in workerLoop.Workers)
                {
                    sb.AppendLine(worker.GetName());
                    AppendLine(sb);
                    worker.UpdateStatus(sb);
                    sb.AppendLine();
                }
                workerLoop.Status = sb.ToString();
            }

            private static void AppendTickHistory(StringBuilder sb)
            {
                sb.AppendLine($"Workers: {workerLoop.Workers.Count}");
                sb.AppendLine($"Last tick time: {workerLoop.LastTickTime} ms");
                sb.AppendLine($"Average load: {workerLoop.AverageTickTime} ms");
                sb.AppendLine("Processing load:");

                StringBuilder temp = new();
                for (int i = 1; i <= AbstractWorkerLoop.TickTimesHistorySize; i++)
                {
                    temp.Append(i.ToString("D2")).Append(new string(' ', 5)); // celkova delka 7
                }
                sb.AppendLine(temp.ToString());
                temp.Clear();

                string[] ticks = workerLoop.TickTimesHistory.Select(i => i.ToString()).ToArray();
                foreach (string tick in ticks)
                {
                    string spaces = new(' ', 7 - tick.Length);
                    temp.Append(tick).Append(spaces);
                }
                sb.AppendLine(temp.ToString()).AppendLine();
            }

            private static void AppendInfo(StringBuilder sb, long elapsedSeconds)
            {
                sb.AppendLine($"World time: {Math.Round(WorldTime.Seconds, 3)}");
                sb.AppendLine($"Elapsed seconds: {elapsedSeconds}");
                sb.AppendLine($"Elapsed time: {TimeSpan.FromSeconds(elapsedSeconds).ToFormatted() ?? "-"}");
                sb.AppendLine();
            }

            private static void AppendLine(StringBuilder sb)
            {
                sb.AppendLine(new string('-', 75));
            }
        }
    }
}
