﻿using Eco.Core;
using Eco.Core.Plugins;
using Eco.Core.Utils;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Messaging.Chat;
using McEco.Lib;
using McEco.MessageHook.Listeners;

namespace McEco.MessageHook
{
    public class InitPlugin : AbstractInitConfigurablePlugin
    {
        public const string DefaultModuleName = "MessageHook";

        private readonly PluginConfig<Config> config = new(DefaultCategory + DefaultModuleName);

        public static InitPlugin Obj => PluginManager.GetPlugin<InitPlugin>();

        public Config Config => config.Config;

        public override IPluginConfig PluginConfig => config;

        public InitPlugin()
        {
            ModuleName = DefaultModuleName;
        }

        public override object GetEditObject()
        {
            return config.Config;
        }

        public override void OnEditObjectChanged(object o, string param)
        {
            config.SaveAsync().Wait();
        }

        protected override void StartUp(TimedTask timed)
        {
            var messageListener = new MessageListener();

            ChatManager.MessageSent.Add(MessageListener.OnMessageSent);
            UserManager.NewUserJoinedEvent.Add(messageListener.NewUserJoinedEvent);
            UserManager.OnUserLoggedIn.Add(messageListener.OnUserLoggedIn);
            UserManager.OnUserLoggedOut.Add(messageListener.OnUserLoggedOut);
        }
    }
}