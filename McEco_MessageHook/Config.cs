﻿using System.ComponentModel;

namespace McEco.MessageHook
{
    public class Config
    {
        [DisplayName("Connections to console")]
        [Description("Log player connections and disconnections to the console.")]
        public bool ConnectionToConsole { get; set; } = true;

        [DisplayName("Messages to console")]
        [Description("Log chat messages to the console.")]
        public bool MessagesToConsole { get; set; } = true;
    }
}
