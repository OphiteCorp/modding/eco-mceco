﻿using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Messaging.Chat;
using McEco.Lib.Ext;
using McEco.Lib.Helper;
using System;
using System.Collections.Generic;
using System.IO;

namespace McEco.MessageHook.Listeners
{
    internal sealed class MessageListener
    {
        private const string TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

        private readonly Dictionary<string, long> users = new();

        public static void OnMessageSent(ChatMessage msg)
        {
            string sender = msg.Sender.Name;
            string receiver = msg.Receiver.Name;
            string category = GetCategory(msg, out bool channel, out bool pm);
            string message = msg.Text;

            if (channel)
            {
                receiver = category;
            }
            if (InitPlugin.Obj.Config.MessagesToConsole)
            {
                $"{sender} -> {(channel ? "#" : "") + receiver}: {message}".ToConsole(InitPlugin.DefaultModuleName);
            }
            WriteMessageToLog(sender, receiver, message, channel, pm);
        }

        public void NewUserJoinedEvent(User user)
        {
            if (user.SlgId == "DiscordLinkSlg")
            {
                return;
            }
            WriteConnectionToLog(user, null, true, true);
        }

        public void OnUserLoggedIn(User user)
        {
            if (user.SlgId == "DiscordLinkSlg")
            {
                return;
            }
            users[user.Name] = DateTime.Now.Ticks;
            WriteConnectionToLog(user, null, true, false);
        }

        public void OnUserLoggedOut(User user)
        {
            if (user.SlgId == "DiscordLinkSlg")
            {
                return;
            }
            TimeSpan? playedFor = null;
            if (users.ContainsKey(user.Name))
            {
                long elapsed = DateTime.Now.Ticks - users[user.Name];
                playedFor = new TimeSpan(elapsed);
            }
            WriteConnectionToLog(user, playedFor, false, false);
        }

        private static string GetCategory(ChatMessage msg, out bool channel, out bool pm)
        {
            string tag = msg.Receiver.ChatTag;
            channel = "#".Equals(tag.Substring(0, 1));
            pm = "@".Equals(tag.Substring(0, 1));
            return tag.Substring(1, tag.Length - 1);
        }

        private static void WriteMessageToLog(string sender, string receiver, string message, bool channel, bool pm)
        {
            string time = DateTime.Now.ToString(TIME_FORMAT);
            string globalLogFile = Path.Combine(InitPlugin.DefaultModuleName, "global.log");
            string logFile;

            if (channel)
            {
                logFile = Path.Combine(InitPlugin.DefaultModuleName, "Channel", $"{receiver}.log");
            }
            else
            {
                logFile = Path.Combine(InitPlugin.DefaultModuleName, "Private", $"{sender}-{receiver}.log");
                string logFileReceiver = Path.Combine(InitPlugin.DefaultModuleName, "Private", $"{receiver}-{sender}.log");

                if (File.Exists(Tools.GetAbsoluteFilePath(GameDir.Logs, logFileReceiver)))
                {
                    logFile = logFileReceiver;
                }
            }
            $"[{time}] {sender}: {message}".WriteToLogFile(logFile);

            if (channel)
            {
                receiver = "#" + receiver;
            }
            $"[{time}] {sender} -> {receiver}: {message}".WriteToLogFile(globalLogFile);
        }

        private static void WriteConnectionToLog(User user, TimeSpan? playedFor, bool connected, bool firstTime)
        {
            string time = DateTime.Now.ToString(TIME_FORMAT);
            string logFile = Path.Combine(InitPlugin.DefaultModuleName, "Connection", $"{user.Name}.log");

            if (connected)
            {
                string detail =
                    $"IP: {user.IpAddress()?.ToString() ?? "-"}, " +
                    $"Slg: {user.SlgId ?? "-"}, Steam: {user.SteamId ?? "-"}, " +
                    $"PlayTime: {user.TotalPlayTimeFormatted()?.ToString()}";

                $"[{time}]: Connected ({detail})".WriteToLogFile(logFile);

                if (InitPlugin.Obj.Config.ConnectionToConsole)
                {
                    if (firstTime)
                    {
                        $"New Connection: {user.Name}, {detail}".ToConsole(InitPlugin.DefaultModuleName);
                    }
                    else
                    {
                        $"Connected: {user.Name}, {detail}".ToConsole(InitPlugin.DefaultModuleName);
                    }
                }
            }
            else
            {
                string detail = $"Played for: {playedFor?.ToFormatted() ?? "-"}";

                $"[{time}]: Disconnected ({detail})".WriteToLogFile(logFile);

                if (InitPlugin.Obj.Config.ConnectionToConsole)
                {
                    $"Disconnected: {user.Name}, {detail}".ToConsole(InitPlugin.DefaultModuleName);
                }
            }
        }
    }
}