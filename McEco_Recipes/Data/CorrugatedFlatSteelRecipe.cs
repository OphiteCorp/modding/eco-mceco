﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(AdvancedSmeltingSkill), 5)]
    public class CorrugatedFlatSteelRecipe : RecipeFamily
    {
        public CorrugatedFlatSteelRecipe()
        {
            var recipe = "Corrugated Flat Steel".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CorrugatedSteelItem), 0.8f, typeof(AdvancedSmeltingSkill), typeof(AdvancedSmeltingLavishResourcesTalent)),
                    new IngredientElement(typeof(EpoxyItem), 2f, typeof(AdvancedSmeltingSkill), typeof(AdvancedSmeltingLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<FlatSteelItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(120, typeof(AdvancedSmeltingSkill));
            ExperienceOnCraft = 1.5f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 0.25f, typeof(AdvancedSmeltingSkill), typeof(AdvancedSmeltingFocusedSpeedTalent), typeof(AdvancedSmeltingParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(RollingMillObject), this);
        }
    }
}