﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(MiningSkill), 2)]
    public class CrushedCoalRecipe : RecipeFamily
    {
        public CrushedCoalRecipe()
        {
            var recipe = "Crushed Coal".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CoalItem), 10, typeof(MiningSkill))
                },
                new CraftingElement[] {
                    new CraftingElement<CrushedCoalItem>(6)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(40, typeof(MiningSkill));
            ExperienceOnCraft = 0;
            CraftMinutes = CreateCraftTimeValue(GetType(), 1.74f, typeof(MiningSkill));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(ArrastraObject), this);
        }
    }
}