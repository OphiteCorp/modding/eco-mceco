﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(FertilizersSkill), 4)]
    public class SpoiledCompostRecipe : RecipeFamily
    {
        public SpoiledCompostRecipe()
        {
            var recipe = "Spoiled Compost".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(SpoiledFoodItem), 10, typeof(FarmingSkill))
                },
                new CraftingElement[] {
                    new CraftingElement<CompostItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(25, typeof(FertilizersSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 1.6f, typeof(FertilizersSkill), typeof(FertilizersFocusedSpeedTalent), typeof(FertilizersParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(FarmersTableObject), this);
        }
    }
}