﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    public class OutpostNettingRecipe : RecipeFamily
    {
        public OutpostNettingRecipe()
        {
            var recipe = "Outpost Netting".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(PlantFibersItem), 22),
                    new IngredientElement("Wood", 4)
                },
                new CraftingElement[] {
                    new CraftingElement<OutpostNettingItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(25);
            CraftMinutes = CreateCraftTimeValue(1);

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(WorkbenchObject), this);
        }
    }
}