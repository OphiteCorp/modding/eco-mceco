﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(PotterySkill), 1)]
    //[RequiresSkill(typeof(MasonrySkill), 7)]
    public class ClayBrickRecipe : RecipeFamily
    {
        public ClayBrickRecipe()
        {
            var recipe = "Clay Brick".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(ClayItem), 200, typeof(PotterySkill), typeof(PotteryLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<BrickItem>(20)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(150, typeof(PotterySkill));
            ExperienceOnCraft = 0.5f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 2, typeof(PotterySkill), typeof(PotteryFocusedSpeedTalent), typeof(PotteryParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(KilnObject), this);
        }
    }
}