﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(GlassworkingSkill), 1)]
    public class SandGlassRecipe : RecipeFamily
    {
        public SandGlassRecipe()
        {
            var recipe = "Sand Glass".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(SandItem), 200, typeof(GlassworkingSkill), typeof(GlassworkingLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<GlassItem>(10)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(150, typeof(GlassworkingSkill));
            ExperienceOnCraft = 0.5f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 2, typeof(GlassworkingSkill), typeof(GlassworkingFocusedSpeedTalent), typeof(GlassworkingParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(KilnObject), this);
        }
    }
}