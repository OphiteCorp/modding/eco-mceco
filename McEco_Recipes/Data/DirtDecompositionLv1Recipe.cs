﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(MiningSkill), 3)]
    public class DirtDecompositionLv1Recipe : RecipeFamily
    {
        public DirtDecompositionLv1Recipe()
        {
            var recipe = "Dirt Decomposition Lv1".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(DirtItem), 25, typeof(MiningSkill))
                },
                new CraftingElement[] {
                    new CraftingElement<ClayItem>(2),
                    new CraftingElement<SandItem>(7),
                    new CraftingElement<CompostItem>(0.5f)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(120, typeof(MiningSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 7, typeof(MiningSkill));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(RockerBoxObject), this);
        }
    }
}