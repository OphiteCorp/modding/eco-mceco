﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(ElectronicsSkill), 4)]
    public class TailingsDecompositionV3Recipe : RecipeFamily
    {
        public TailingsDecompositionV3Recipe()
        {
            var recipe = "Decomposition of Tailings Lv3".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(TailingsItem), 1, true)
                },
                new CraftingElement[] {
                    new CraftingElement<CrushedCopperOreItem>(0.55f),
                    new CraftingElement<CrushedGoldOreItem>(0.4f),
                    new CraftingElement<CrushedIronOreItem>(0.7f),
                    new CraftingElement<TailingsItem>(3.5f)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(130, typeof(ElectronicsSkill));
            ExperienceOnCraft = 0;
            CraftMinutes = CreateCraftTimeValue(GetType(), 4.5f, typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(JawCrusherObject), this);
        }
    }
}