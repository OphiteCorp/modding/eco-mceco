﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(TailoringSkill), 1)]
    public class CelluloseClothRecipe : RecipeFamily
    {
        public CelluloseClothRecipe()
        {
            var recipe = "Cellulose Cloth".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CelluloseFiberItem), 12, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<ClothItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(80, typeof(TailoringSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 1, typeof(TailoringSkill), typeof(TailoringFocusedSpeedTalent), typeof(TailoringParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(TailoringTableObject), this);
        }
    }
}