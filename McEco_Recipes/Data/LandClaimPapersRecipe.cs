﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(PaperMillingSkill), 7)]
    public class LandClaimPapersRecipe : RecipeFamily
    {
        public LandClaimPapersRecipe()
        {
            var recipe = "Land Claim Papers".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(PaperItem), 400, true),
                    new IngredientElement(typeof(SubstrateItem), 100, true),
                    new IngredientElement(typeof(GoldWiringItem), 400, true),
                    new IngredientElement(typeof(CopperWiringItem), 400, true),
                    new IngredientElement(typeof(LiquidNitrogenItem), 50, true)
                },
                new CraftingElement[] {
                    new CraftingElement<PropertyClaimItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(100000, typeof(PaperMillingSkill));
            ExperienceOnCraft = 0f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 30f, typeof(PaperMillingSkill));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(CapitolObject), this);
        }
    }
}