﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(MasonrySkill), 7)]
    public class SandyLimestoneRecipe : RecipeFamily
    {
        public SandyLimestoneRecipe()
        {
            var recipe = "Sandy Limestone".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(SlagItem), 8, typeof(MechanicsSkill), typeof(MechanicsLavishResourcesTalent)),
                    new IngredientElement(typeof(SandItem), 0.2f, typeof(MasonrySkill), typeof(MasonryLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<LimestoneItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(40, typeof(MasonrySkill));
            ExperienceOnCraft = 0.5f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 0.55f, typeof(MasonrySkill), typeof(MasonryFocusedSpeedTalent), typeof(MasonryParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(MasonryTableObject), this);
        }
    }
}