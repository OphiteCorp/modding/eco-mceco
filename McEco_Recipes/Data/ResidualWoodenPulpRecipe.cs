﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    public class ResidualWoodenPulpRecipe : RecipeFamily
    {
        public ResidualWoodenPulpRecipe()
        {
            var recipe = "Residual Wooden Pulp".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement("Wood", 1, typeof(LoggingSkill))
                },
                new CraftingElement[] {
                    new CraftingElement<WoodPulpItem>(10)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(20);
            CraftMinutes = CreateCraftTimeValue(0.35f);

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(WorkbenchObject), this);
        }
    }
}