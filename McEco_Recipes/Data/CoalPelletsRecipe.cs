﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(MiningSkill), 6)]
    public class CoalPelletsRecipe : RecipeFamily
    {
        public CoalPelletsRecipe()
        {
            var recipe = "Coal Pellets".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CharcoalItem), 1, typeof(MiningSkill)),
                    new IngredientElement(typeof(OilItem), 0.2f, typeof(MiningSkill))
                },
                new CraftingElement[] {
                    new CraftingElement<CoalItem>(3)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(100, typeof(MiningSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 1.7f, typeof(MiningSkill));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(RockerBoxObject), this);
        }
    }
}