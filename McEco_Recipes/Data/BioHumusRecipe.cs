﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(OilDrillingSkill), 7)]
    public class BioHumusRecipe : RecipeFamily
    {
        public BioHumusRecipe()
        {
            var recipe = "Bio Humus".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CompostItem), 25, typeof(OilDrillingSkill), typeof(OilDrillingLavishResourcesTalent)),
                    new IngredientElement(typeof(OilItem), 2, typeof(OilDrillingSkill), typeof(OilDrillingLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<BiodieselItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(70, typeof(OilDrillingSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 3.5f, typeof(OilDrillingSkill), typeof(OilDrillingFocusedSpeedTalent), typeof(OilDrillingParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(OilRefineryObject), this);
        }
    }
}