﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(GlassworkingSkill), 5)]
    public class CorrugatedFramedGlassRecipe : RecipeFamily
    {
        public CorrugatedFramedGlassRecipe()
        {
            var recipe = "Corrugated Framed Glass".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CorrugatedSteelItem), 0.9f, typeof(GlassworkingSkill), typeof(GlassworkingLavishResourcesTalent)),
                    new IngredientElement(typeof(GlassItem), 4f, typeof(GlassworkingSkill), typeof(GlassworkingLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<FramedGlassItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(120, typeof(GlassworkingSkill));
            ExperienceOnCraft = 1.5f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 0.25f, typeof(GlassworkingSkill), typeof(GlassworkingFocusedSpeedTalent), typeof(GlassworkingParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(GlassworksObject), this);
        }
    }
}