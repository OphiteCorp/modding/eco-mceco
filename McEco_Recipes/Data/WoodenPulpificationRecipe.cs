﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(CarpentrySkill), 2)]
    public class WoodenPulpificationRecipe : RecipeFamily
    {
        public WoodenPulpificationRecipe()
        {
            var recipe = "Wooden Pulpification".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(WoodPulpItem), 20, typeof(CarpentrySkill), typeof(CarpentryLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<HewnLogItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(10, typeof(CarpentrySkill));
            ExperienceOnCraft = 0.03f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 0.45f, typeof(CarpentrySkill), typeof(CarpentryFocusedSpeedTalent), typeof(CarpentryParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(CarpentryTableObject), this);
        }
    }
}