﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(TailoringSkill), 7)]
    public class VipSuitRecipe : RecipeFamily
    {
        public VipSuitRecipe()
        {
            var recipe = "VIP Suit".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(MeteorShardItem), 1, true),
                    new IngredientElement(typeof(EcoylentItem), 5, true),
                    new IngredientElement(typeof(NylonFabricItem), 100, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                    new IngredientElement(typeof(FiberglassItem), 60, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                    new IngredientElement(typeof(GoldWiringItem), 90, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                },
                new CraftingElement[] {
                    new CraftingElement<MeteorShardItem>(1),
                    new CraftingElement<WolfWhispererHatItem>(1),
                    new CraftingElement<WolfWhispererMaskItem>(1),
                    new CraftingElement<WolfWhispererShirtItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(12500, typeof(TailoringSkill));
            ExperienceOnCraft = 1;
            CraftMinutes = CreateCraftTimeValue(GetType(), 45, typeof(TailoringSkill), typeof(TailoringFocusedSpeedTalent), typeof(TailoringParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(AutomaticLoomObject), this);
        }
    }
}