﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(ElectronicsSkill), 2)]
    public class TailingsNitrogenationRecipe : RecipeFamily
    {
        public TailingsNitrogenationRecipe()
        {
            var recipe = "Nitrogenation of Tailings".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(TailingsItem), 4, typeof(ElectronicsSkill), typeof(ElectronicsLavishResourcesTalent)),
                    new IngredientElement(typeof(LiquidNitrogenItem), 1, typeof(ElectronicsSkill), typeof(ElectronicsLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<WetTailingsItem>(25)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(40, typeof(ElectronicsSkill));
            ExperienceOnCraft = 0.05f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 12.25f, typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(FrothFloatationCellObject), this);
        }
    }
}