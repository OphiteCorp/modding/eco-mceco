﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(CuttingEdgeCookingSkill), 7)]
    public class ElixirOfLifeRecipe : RecipeFamily
    {
        public ElixirOfLifeRecipe()
        {
            var recipe = "Elixir of Life".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(BanhXeoItem), 4, true),
                    new IngredientElement(typeof(StuffedTurkeyItem), 4, true),
                    new IngredientElement(typeof(PhadThaiItem), 4, true),
                    new IngredientElement(typeof(UrchinItem), 3, true),
                    new IngredientElement(typeof(GasolineItem), 8, true),
                    new IngredientElement(typeof(LiquidNitrogenItem), 25, typeof(CuttingEdgeCookingSkill), typeof(CuttingEdgeCookingLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<EcoylentItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(110, typeof(CuttingEdgeCookingSkill));
            ExperienceOnCraft = 0.2f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 5, typeof(CuttingEdgeCookingSkill), typeof(CuttingEdgeCookingFocusedSpeedTalent), typeof(CuttingEdgeCookingParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(LaboratoryObject), this);
        }
    }
}