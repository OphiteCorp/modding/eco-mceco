﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(ElectronicsSkill), 4)]
    public class TailingsRecyclingRecipe : RecipeFamily
    {
        public TailingsRecyclingRecipe()
        {
            var recipe = "Chemical Tailings Recycling".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(TailingsItem), 20, typeof(ElectronicsSkill), typeof(ElectronicsLavishResourcesTalent)),
                    new IngredientElement(typeof(SandItem), 3,  typeof(MasonrySkill), typeof(MasonryLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<CharcoalItem>(0.75f)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(35, typeof(ElectronicsSkill));
            ExperienceOnCraft = 0.05f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 2.25f, typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(LaboratoryObject), this);
        }
    }
}