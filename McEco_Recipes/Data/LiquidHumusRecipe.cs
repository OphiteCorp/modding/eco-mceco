﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(FertilizersSkill), 3)]
    public class LiquidHumusRecipe : RecipeFamily
    {
        public LiquidHumusRecipe()
        {
            var recipe = "Liquid Humus".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CompostItem), 1, typeof(FertilizersSkill), typeof(FertilizersLavishResourcesTalent)),
                    new IngredientElement(typeof(SandItem), 3, typeof(MiningSkill), typeof(FertilizersLavishResourcesTalent)),
                    new IngredientElement(typeof(ClayItem), 1, typeof(MiningSkill), typeof(FertilizersLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<OilItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(120, typeof(FertilizersSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 7, typeof(FertilizersSkill), typeof(FertilizersFocusedSpeedTalent), typeof(FertilizersParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(FarmersTableObject), this);
        }
    }
}