﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(BasicEngineeringSkill), 4)]
    public class TailingsDecompositionV1Recipe : RecipeFamily
    {
        public TailingsDecompositionV1Recipe()
        {
            var recipe = "Decomposition of Tailings Lv1".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(WetTailingsItem), 1, true)
                },
                new CraftingElement[] {
                    new CraftingElement<CrushedCopperOreItem>(0.25f),
                    new CraftingElement<CrushedGoldOreItem>(0.1f),
                    new CraftingElement<CrushedIronOreItem>(0.4f),
                    new CraftingElement<TailingsItem>(2)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(90, typeof(BasicEngineeringSkill));
            ExperienceOnCraft = 0;
            CraftMinutes = CreateCraftTimeValue(GetType(), 10.5f, typeof(BasicEngineeringSkill), typeof(BasicEngineeringFocusedSpeedTalent), typeof(BasicEngineeringParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(ArrastraObject), this);
        }
    }
}