﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(BasicEngineeringSkill), 6)]
    public class SlagRecipe : RecipeFamily
    {
        public SlagRecipe()
        {
            var recipe = "Slag".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement("CrushedRock", 2, typeof(MiningSkill)),
                    new IngredientElement(typeof(CoalItem), 1, typeof(MiningSkill))
                },
                new CraftingElement[] {
                    new CraftingElement<SlagItem>(2)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(40, typeof(MiningSkill));
            ExperienceOnCraft = 0.01f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 0.75f, typeof(MiningSkill));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(RockerBoxObject), this);
        }
    }
}