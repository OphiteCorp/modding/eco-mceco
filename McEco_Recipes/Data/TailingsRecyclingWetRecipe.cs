﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(ElectronicsSkill), 7)]
    public class TailingsRecyclingWetRecipe : RecipeFamily
    {
        public TailingsRecyclingWetRecipe()
        {
            var recipe = "Chemical Wet Tailings Recycling".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(WetTailingsItem), 20, typeof(ElectronicsSkill), typeof(ElectronicsLavishResourcesTalent)),
                    new IngredientElement(typeof(ClayItem), 3, typeof(MasonrySkill), typeof(MasonryLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<BiodieselItem>(0.35f)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(35, typeof(ElectronicsSkill));
            ExperienceOnCraft = 0.05f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 4.65f, typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(LaboratoryObject), this);
        }
    }
}