﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(ElectronicsSkill), 7)]
    public class HydrogenCyanideRecipe : RecipeFamily
    {
        public HydrogenCyanideRecipe()
        {
            var recipe = "Hydrogen Cyanide".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CoalItem), 200, true),
                    new IngredientElement(typeof(CharcoalItem), 200, true),
                    new IngredientElement(typeof(BiodieselItem), 100, true),
                    new IngredientElement(typeof(GasolineItem), 100, true),
                    new IngredientElement(typeof(OilItem), 200, true)
                },
                new CraftingElement[] {
                    new CraftingElement<StrangeFuelItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(8500, typeof(ElectronicsSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 16f, typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(LaboratoryObject), this);
        }
    }
}