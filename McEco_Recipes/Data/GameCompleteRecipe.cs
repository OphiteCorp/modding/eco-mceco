﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(ElectronicsSkill), 7)]
    public class GameCompleteRecipe : RecipeFamily
    {
        public GameCompleteRecipe()
        {
            var recipe = "Game complete. Congratulations!".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(ComputerLabItem), 1, true),
                    new IngredientElement(typeof(LaserItem), 1, true),
                    new IngredientElement(typeof(SkidSteerItem), 1, true),
                    new IngredientElement(typeof(CombustionEngineItem), 5, true),
                    new IngredientElement(typeof(NylonFabricItem), 60, true),
                    new IngredientElement(typeof(CompositesUpgradeItem), 1, true),
                    new IngredientElement(typeof(CuttingEdgeCookingUpgradeItem), 1, true),
                    new IngredientElement(typeof(EckoStatueItem), 1, true)
                },
                new CraftingElement[] {
                    new CraftingElement<MeteorShardItem>(1),
                    new CraftingElement<AlphaHatItem>(1),
                    new CraftingElement<AlphaGogglesItem>(1),
                    new CraftingElement<HydrometerItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(40000, typeof(ElectronicsSkill));
            ExperienceOnCraft = 1;
            CraftMinutes = CreateCraftTimeValue(GetType(), 60, typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(RoboticAssemblyLineObject), this);
        }
    }
}