﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(FertilizersSkill), 4)]
    public class FertileClayRecipe : RecipeFamily
    {
        public FertileClayRecipe()
        {
            var recipe = "Fertile Clay".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CompostItem), 1, typeof(FertilizersSkill), typeof(FertilizersLavishResourcesTalent)),
                    new IngredientElement(typeof(SandItem), 1, typeof(MiningSkill), typeof(FertilizersLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<ClayItem>(2),
                    new CraftingElement<DirtItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(15, typeof(FertilizersSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 0.85f, typeof(FertilizersSkill), typeof(FertilizersFocusedSpeedTalent), typeof(FertilizersParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(FarmersTableObject), this);
        }
    }
}