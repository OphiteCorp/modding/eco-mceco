﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(AdvancedSmeltingSkill), 7)]
    public class CompoundTailingsRecipe : RecipeFamily
    {
        public CompoundTailingsRecipe()
        {
            var recipe = "Compound Tailings".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(WetTailingsItem), 5, typeof(MiningSkill)),
                    new IngredientElement(typeof(CompostItem), 5, typeof(FarmingSkill))
                },
                new CraftingElement[] {
                    new CraftingElement<TailingsItem>(20)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(50, typeof(AdvancedSmeltingSkill));
            ExperienceOnCraft = 0.5f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 6.3f, typeof(AdvancedSmeltingSkill), typeof(AdvancedSmeltingFocusedSpeedTalent), typeof(AdvancedSmeltingParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(BlastFurnaceObject), this);
        }
    }
}