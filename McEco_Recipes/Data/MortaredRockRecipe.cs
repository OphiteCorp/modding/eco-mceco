﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(MasonrySkill), 1)]
    //[RequiresSkill(typeof(MiningSkill), 7)]
    public class MortaredRockRecipe : RecipeFamily
    {
        public MortaredRockRecipe()
        {
            var recipe = "Mortared Rock".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement("Rock", 200, typeof(MasonrySkill), typeof(MasonryLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<MortaredStoneItem>(20)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(150, typeof(MasonrySkill));
            ExperienceOnCraft = 0.5f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 2, typeof(MasonrySkill), typeof(MasonryFocusedSpeedTalent), typeof(MasonryParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(MasonryTableObject), this);
        }
    }
}