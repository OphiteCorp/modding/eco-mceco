﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(AdvancedMasonrySkill), 7)]
    public class EckoStatueRecipe : RecipeFamily
    {
        public EckoStatueRecipe()
        {
            var recipe = "Ecko Statue".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(ReinforcedConcreteItem), 300, typeof(AdvancedMasonrySkill), typeof(AdvancedMasonryLavishResourcesTalent)),
                    new IngredientElement(typeof(AdvancedCircuitItem), 10, typeof(ElectronicsSkill), typeof(ElectronicsLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<EckoStatueItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(850, typeof(AdvancedMasonrySkill));
            ExperienceOnCraft = 0.5f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 21f, typeof(AdvancedMasonrySkill), typeof(AdvancedMasonryFocusedSpeedTalent), typeof(AdvancedMasonryParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(RoboticAssemblyLineObject), this);
        }
    }
}