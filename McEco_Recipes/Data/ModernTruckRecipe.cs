﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(IndustrySkill), 6)]
    public class ModernTruckRecipe : RecipeFamily
    {
        public ModernTruckRecipe()
        {
            var recipe = "Modern Truck".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(GearboxItem), 8f, typeof(IndustrySkill)),
                    new IngredientElement(typeof(SteelPlateItem), 40f, typeof(IndustrySkill)),
                    new IngredientElement(typeof(NylonFabricItem), 40f, typeof(IndustrySkill)),
                    new IngredientElement(typeof(CombustionEngineItem), 4f, staticIngredient: true),
                    new IngredientElement(typeof(RubberWheelItem), 12f, staticIngredient: true),
                    new IngredientElement(typeof(RadiatorItem), 4f, staticIngredient: true),
                    new IngredientElement(typeof(SteelAxleItem), 8f, staticIngredient: true),
                    new IngredientElement(typeof(LightBulbItem), 6f, staticIngredient: true)
                },
                new CraftingElement[] {
                    new CraftingElement<TrailerTruckItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(8000f, typeof(IndustrySkill));
            ExperienceOnCraft = 18f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 14f, typeof(IndustrySkill), typeof(IndustryFocusedSpeedTalent), typeof(IndustryParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(RoboticAssemblyLineObject), this);
        }
    }
}