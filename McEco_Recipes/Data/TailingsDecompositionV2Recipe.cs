﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(MechanicsSkill), 4)]
    public class TailingsDecompositionV2Recipe : RecipeFamily
    {
        public TailingsDecompositionV2Recipe()
        {
            var recipe = "Decomposition of Tailings Lv2".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(TailingsItem), 1, true)
                },
                new CraftingElement[] {
                    new CraftingElement<CrushedCopperOreItem>(0.4f),
                    new CraftingElement<CrushedGoldOreItem>(0.25f),
                    new CraftingElement<CrushedIronOreItem>(0.55f),
                    new CraftingElement<TailingsItem>(2.75f)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(110, typeof(MechanicsSkill));
            ExperienceOnCraft = 0;
            CraftMinutes = CreateCraftTimeValue(GetType(), 7.5f, typeof(MechanicsSkill), typeof(MechanicsFocusedSpeedTalent), typeof(MechanicsParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(StampMillObject), this);
        }
    }
}