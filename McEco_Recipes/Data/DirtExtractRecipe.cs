﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(FertilizersSkill), 6)]
    public class DirtExtractRecipe : RecipeFamily
    {
        public DirtExtractRecipe()
        {
            var recipe = "Dirt Extract".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(DirtItem), 1, typeof(MiningSkill), typeof(FertilizersLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<ClayItem>(0.5f),
                    new CraftingElement<CompostItem>(0.25f)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(15, typeof(FertilizersSkill));
            ExperienceOnCraft = 0.1f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 0.85f, typeof(FertilizersSkill), typeof(FertilizersFocusedSpeedTalent), typeof(FertilizersParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(FarmersTableObject), this);
        }
    }
}