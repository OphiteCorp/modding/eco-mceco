﻿using System.Collections.Generic;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using McEco.Lib.Ext;

namespace McEco.Recipes.Data
{
    [RequiresSkill(typeof(MillingSkill), 3)]
    public class CottonSeedOilRecipe : RecipeFamily
    {
        public CottonSeedOilRecipe()
        {
            var recipe = "Cotton Seed Oil".CreateRecipe(
                new IngredientElement[] {
                    new IngredientElement(typeof(CottonSeedItem), 50, typeof(MillingSkill), typeof(MillingLavishResourcesTalent))
                },
                new CraftingElement[] {
                    new CraftingElement<OilItem>(1)
            });

            Recipes = new List<Recipe> { recipe };
            LaborInCalories = CreateLaborInCaloriesValue(20, typeof(MillingSkill));
            ExperienceOnCraft = 0.3f;
            CraftMinutes = CreateCraftTimeValue(GetType(), 2, typeof(MillingSkill), typeof(MillingFocusedSpeedTalent), typeof(MillingParallelSpeedTalent));

            Initialize(recipe.DisplayName, GetType());
            CraftingComponent.AddRecipe(typeof(MillObject), this);
        }
    }
}