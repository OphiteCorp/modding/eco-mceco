﻿using Eco.Core.Utils;
using McEco.Lib;

namespace McEco.Recipes
{
    public class InitPlugin : AbstractInitPlugin
    {
        public const string DefaultModuleName = "Recipes";

        public InitPlugin()
        {
            ModuleName = DefaultModuleName;
        }

        protected override void StartUp(TimedTask timed)
        {
        }
    }
}